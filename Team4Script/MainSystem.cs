﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class MainSystem : MonoBehaviour
{
    public static MainSystem Instance { get; private set; }
    //地圖系統
    public MapSystem CMapSystem { get; private set; }
    //戰鬥系統
    public BattleSystem CBattleSystem { get; private set; }
    //資源池
    public AssetsPool CAssetsPool { get; private set; }
    //場景管理器
    public SceneSystem CSceneSystem { get; private set; }
    //音效系統
    public AudioSystem CAudioSystem { get; private set; }
    //特效系統
    public EffectSystem CEffectSystem { get; private set; }
    //HUD系統
    //public UISystem CUISystem { get; private set; }
    public HUDSystem CHUDSystem { get; private set; }
    //AI系統
    public AI_System CAI_System { get; private set; }
    //BGM
    public BgmSystem CBgmSystem { get; private set; }

    //遊戲輸入(根據遊戲狀態判定)
    public GameStateInput CGameStateInput { get; private set; }
    //遊戲紀錄
    public RecordData CRecordData { get; private set; }

    //遊戲狀態
    public GameState EGameState { get; set; }

    public delegate void GameStateEvent();
    private event GameStateEvent m_NewEvent;
    private event GameStateEvent m_LoadEvent;

    public bool IsOneCall { get; set; }
    public GameObject HUDCanvas { get; set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    private void OnEnable()
    {
        CAssetsPool = new AssetsPool();
        CSceneSystem = new SceneSystem();
        CBattleSystem = new BattleSystem();
        CGameStateInput = new GameStateInput();
        CBgmSystem = new BgmSystem();
        //CUISystem = new UISystem();
        CHUDSystem = new HUDSystem();
        CRecordData = new RecordData();
        IsOneCall = false;
    }

    private void Start()
    {
        m_NewEvent += CRecordData.ResetSaveData;
        m_NewEvent += InitObjects;
        m_NewEvent += CBattleSystem.ResetStatusList;
        m_NewEvent += CGameStateInput.Reset;

        m_LoadEvent += InitObjects;
        m_LoadEvent += CBattleSystem.ResetStatusList;
        m_LoadEvent += CGameStateInput.Reset;
    }

    private void Update()
    {
        switch (EGameState)
        {
            case GameState.WELCOMEVIEW:
                break;
            case GameState.GAMEPLAY:
                TestInput();
                CGameStateInput.Menu();
                CGameStateInput.ChangeItem();
                CBgmSystem.SceneMusic();
                CheckCompleteCondition();
                break;
            case GameState.GAMEOVER:
                if(IsOneCall == false)
                {
                    CAssetsPool.ShowDeathWindow();
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    IsOneCall = true;
                }
                break;
            case GameState.GAMECOMPLETE:
                if (IsOneCall == false)
                {
                    CAssetsPool.UISources[Window.BOSS_KILLED].SetActive(true);
                    IsOneCall = true;
                }
                break;
            case GameState.NEWGAMEMOVIE:
                if (IsOneCall == false)
                {
                    CGameStateInput.IsStopPlayerInput = true;
                    CAssetsPool.MainCamera.gameObject.SetActive(false);
                    CAssetsPool.UISources[Window.GAMEPLAY_WINDOW].SetActive(false);
                    CAssetsPool.TimeLineBoss.SendMessage("TargetMovieClip", MovieClip.Open);
                    IsOneCall = true;
                }
                break;
        }
            
    }

    void InitObjects()
    {
        CAssetsPool.MainCamera = Camera.main;
        CAssetsPool.MainCameraControl = CAssetsPool.MainCamera.GetComponent<CameraControl>();

        CAssetsPool.Characters.Clear();
        CAssetsPool.AudioPlayers.Clear();
        CAssetsPool.EffectPlayerQueues.Clear();
        CAssetsPool.EffectPlayerQueue1.Clear();
        CAssetsPool.EffectPlayerQueue2.Clear();
        CAssetsPool.EffectPlayerQueue3.Clear();
        CAssetsPool.EffectPlayerQueue4.Clear();
        CAssetsPool.EffectPlayerQueue5.Clear();
        CAssetsPool.EffectPlayerQueue6.Clear();
        CAssetsPool.EffectPlayerQueue7.Clear();
        CAssetsPool.EffectPlayerQueue8.Clear();
        CAssetsPool.HUDSources.Clear();

        ////創造玩家
        var player = Instantiate(CAssetsPool.CharactersAssets["MainPlayer"], CRecordData.SavePostion, CRecordData.SaveRotation);
        CAssetsPool.Characters.Add(player);
        
        
        CBgmSystem.BgmPlayer.Obj = Instantiate(CAssetsPool.AudioPlayerAsset);
        CBgmSystem.BgmPlayer.Source = CBgmSystem.BgmPlayer.Obj.GetComponent<AudioSource>();
        CAudioSystem = Instantiate(CAssetsPool.AudioSystem).GetComponent<AudioSystem>();
        CEffectSystem = Instantiate(CAssetsPool.EffectSystem).GetComponent<EffectSystem>();

        CAssetsPool.UISources.Clear();
        CAssetsPool.UISources.Add(Window.MAIN_WINDOW, Instantiate(CAssetsPool.UIAssets["MainWindow"]));
        CAssetsPool.UISources.Add(Window.DEATH_WINDOW, CAssetsPool.UISources[Window.MAIN_WINDOW].transform.Find("DeathWindow").gameObject);
        CAssetsPool.UISources.Add(Window.MAIN_MENU_WINDOW, CAssetsPool.UISources[Window.MAIN_WINDOW].transform.Find("MainMenuWindow").gameObject);
        CAssetsPool.UISources.Add(Window.EQUIT_WINDOW, CAssetsPool.UISources[Window.MAIN_MENU_WINDOW].transform.Find("EquitWindow").gameObject);
        CAssetsPool.UISources.Add(Window.SETTINGS_WINDOW, CAssetsPool.UISources[Window.MAIN_MENU_WINDOW].transform.Find("SettingsWindow").gameObject);
        CAssetsPool.UISources.Add(Window.ITEM_SELECT_WINDOW, CAssetsPool.UISources[Window.MAIN_MENU_WINDOW].transform.Find("ItemSelectWindow").gameObject);
        CAssetsPool.UISources.Add(Window.SKILL_SELECT_WINDOW, CAssetsPool.UISources[Window.MAIN_MENU_WINDOW].transform.Find("SkillSelectWindow").gameObject);
        CAssetsPool.UISources.Add(Window.CONFIRM_WINDOW, CAssetsPool.UISources[Window.MAIN_MENU_WINDOW].transform.Find("ConfirmWindow").gameObject);
        CAssetsPool.UISources.Add(Window.GAMEPLAY_WINDOW, CAssetsPool.UISources[Window.MAIN_WINDOW].transform.Find("GamePlayWindow").gameObject);
        CAssetsPool.UISources.Add(Window.LOAD_WINDOW, CAssetsPool.UISources[Window.MAIN_WINDOW].transform.Find("LoadWindow").gameObject);
        CAssetsPool.UISources.Add(Window.AREA_WINDOW, CAssetsPool.UISources[Window.MAIN_WINDOW].transform.Find("AreaWindow").gameObject);
        CAssetsPool.UISources.Add(Window.OPTION_VOLUME_WINDOW, CAssetsPool.UISources[Window.SETTINGS_WINDOW].transform.Find("ViewSetting").gameObject);
        CAssetsPool.UISources.Add(Window.BLACK_FADE, CAssetsPool.UISources[Window.MAIN_WINDOW].transform.Find("BlackFade").gameObject);
        CAssetsPool.UISources.Add(Window.BOSS_KILLED, CAssetsPool.UISources[Window.MAIN_WINDOW].transform.Find("KillWindow").gameObject);

        // 創建 HUD Prefab
        HUDCanvas=Instantiate(CAssetsPool.HUDAssets[HUD_Name.HUD_CANVAS]);

        //創造玩家血條UI
        var hudPlayer = Instantiate(CAssetsPool.HUDAssets[HUD_Name.HUD_MAIN_PLAYER]);
        CAssetsPool.HUDSources.Add(player.transform, hudPlayer.GetComponent<HUD_MainPlayerPerformance>());
        //hudPlayer.transform.SetParent(CAssetsPool.UISources[Window.MAIN_WINDOW].transform);
        //hudPlayer.transform.SetParent(HUDCanvas.transform);
        CAI_System = Instantiate(CAssetsPool.AI_System).GetComponent<AI_System>();

        CAssetsPool.ObjectsPointer = CAssetsPool.UISources[Window.MAIN_WINDOW].GetComponent<UIObjectsPointer>();

        CAssetsPool.TimeLineBoss = GameObject.FindGameObjectWithTag("TimeLine");

        CAssetsPool.ObjectsPointer.GamePlayMissionText.text = CRecordData.CurrentMission[0];
    }

    public void InitSystem(AsyncOperation async)
    {
        if(CMapSystem == null) CMapSystem = new MapSystem();
        m_NewEvent();
        EGameState = GameState.NEWGAMEMOVIE;
        IsOneCall = false;
    }

    public void LoadSystem(AsyncOperation async)
    {
        m_LoadEvent();
        EGameState = GameState.GAMEPLAY;
        IsOneCall = false;
    }

    public void WelcomeSceneSet(AsyncOperation async)
    {
        EGameState = GameState.WELCOMEVIEW;
        CGameStateInput.IsStopPlayerInput = false;
    }

    void CheckCompleteCondition()
    {
        bool isGameComplete = false;
        foreach (AIStatus status in CAI_System.statusList)
        {
            isGameComplete = isGameComplete || (status.Rank == AI_Rank.STAGE_BOSS && status.VitalStatus == AI_VitalStatus.DEAD);
        }
        if(isGameComplete)
        {
            CBgmSystem.ForceStop();
            CAudioSystem.PlayAudio(AudioName.EXECUTION, 0.5f);
            IsOneCall = false;
            EGameState = GameState.GAMECOMPLETE;
        }
    }

    void TestInput()
    {
        //if (Input.GetKeyDown(KeyCode.F5))
        //{
        //    ItemInfo info = new ItemInfo(ItemType.HEAL, "HealBottle", CAssetsPool.ImageAssets[AssetsSprite.HEAL_BOTTLE], "傷藥葫蘆", 10, 10, 0, 0, 0,
        //        "裝著可回復HP之傷藥的葫蘆\n經過休息之後，葫蘆裡的靈藥便會再度填滿\n\n由稀世藥師．道玄\n其弟子之一所製成\n有著能自動湧出藥水等等奇異之處" +
        //        "\n但箇中自有其玄機", EffectName.HEAL_WAVE1, AudioName.HEAL);
        //    CAssetsPool.ObjectsPointer.AddNewItem(info, true);
        //}
        if (Input.GetKeyDown(KeyCode.F5))
        {
            ItemInfo info = new ItemInfo(ItemType.HEAL, "HealDrug", CAssetsPool.ImageAssets[AssetsSprite.HEAL_DRUG], "藥丸", 10, 10, 0, 0, 0,
                "能逐漸回復HP的藥丸\n" +
                "自古便於此地流傳的祕藥\n" +
                "在遠古戰事中便有使用的紀錄，\n",
                EffectName.HEAL_WAVE1, AudioName.HEAL);
            CAssetsPool.ObjectsPointer.AddNewItem(info, true);
        }
        if (Input.GetKeyDown(KeyCode.F6))
        {
            ItemInfo info = new ItemInfo(ItemType.ATTACK_UP_TEMP, "AttackDrug", CAssetsPool.ImageAssets[AssetsSprite.ATTACKSUGER], "阿攻糖", 5, 0, 2, 0, 30,
                "能獲得庇護的佛糖\n" +
                "可暫時提高攻擊力與對軀幹的攻擊力\n" +
                "只要咬碎這種糖，\n" +
                "即可使超常御靈之庇護降於己身\n" +
                "\n",
                EffectName.ATTACK_BUFF, AudioName.BUFF);
            CAssetsPool.ObjectsPointer.AddNewItem(info, true);
        }

    }


}
