﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIButtonPointer : MonoBehaviour, IPointerEnterHandler
{
    private MainSystem m_MainSystem = null;

    public string ObjectType { get; set; }

    public Sprite Icon { get; set; }
    public string ObjectName { get; set; }
    public int ItemCount { get; private set; }
    public SkillInfo SelectInfo = null;

    private Image UIIcon = null;
    private Text UIName = null;
    private Text UICount = null;

    public void Init()
    {
        m_MainSystem = MainSystem.Instance;
        switch (ObjectType)
        {
            case "SkillButton":
                transform.Find("Image").GetComponent<Image>().sprite = m_MainSystem.CAssetsPool.ImageAssets[AssetsSprite.SKILL_BG];
                break;
            case "ItemButton":
                transform.Find("Image").GetComponent<Image>().sprite = m_MainSystem.CAssetsPool.ImageAssets[AssetsSprite.ITEM_BG];
                break;
        }
        UIIcon = transform.Find("Image/MainImage").GetComponent<Image>();
        UIName = transform.Find("Button/Text").GetComponent<Text>();
        UICount = transform.Find("Image/Text").GetComponent<Text>();

        ItemCount = 0;
    }

    public void InitUI()
    {
        UIIcon.sprite = Icon;
        UIName.text = ObjectName;
        if(ObjectType == "ItemButton")
        {
            UICount.text = ItemCount.ToString();
            ChangeItemColor(Color.gray);
        }
    }

    public void AddItemCount()
    {
        ItemCount++;
        UICount.text = ItemCount.ToString();
        if(Icon == m_MainSystem.CAssetsPool.ObjectsPointer.GamePlayItemImage.sprite)
            m_MainSystem.CAssetsPool.ObjectsPointer.GamePlayItemText.text = ItemCount.ToString();
        if(UIIcon.color == Color.gray) ChangeItemColor(Color.white);
    }

    public void MinusItemCount()
    {
        ItemCount--;
        UICount.text = ItemCount.ToString();
        m_MainSystem.CAssetsPool.ObjectsPointer.GamePlayItemText.text = ItemCount.ToString();
        if(ItemCount <= 0) ChangeItemColor(Color.gray);
    }

    void ChangeItemColor(Color color)
    {
        m_MainSystem.CAssetsPool.ObjectsPointer.ImageColorChange(this.gameObject.name, m_MainSystem.CAssetsPool.ObjectsPointer.GamePlayItemImage);
        UIIcon.color = color;
        foreach (var o in m_MainSystem.CAssetsPool.ObjectsPointer.EquitItemButtonImage)
        {
            if (o.sprite == UIIcon.sprite) o.color = color;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        switch (ObjectType)
        {
            case "SkillButton":
                m_MainSystem.CAssetsPool.ObjectsPointer.SkillListDepiction.text = SelectInfo.Deciption;
                m_MainSystem.CAssetsPool.ObjectsPointer.SkillListTitle.text = SelectInfo.SkillName;
                break;
            case "ItemButton":
                if (Icon != null)
                {
                    if (m_MainSystem.CAssetsPool.ObjectsPointer.ItemDictionary.ContainsKey(Icon) == true)
                    {
                        m_MainSystem.CAssetsPool.ObjectsPointer.ItemListDepiction.text = m_MainSystem.CAssetsPool.ObjectsPointer.ItemDictionary[Icon].Depiction;
                        m_MainSystem.CAssetsPool.ObjectsPointer.ItemListTitle.text = m_MainSystem.CAssetsPool.ObjectsPointer.ItemDictionary[Icon].ItemName;
                        m_MainSystem.CAssetsPool.ObjectsPointer.ItemListImage.sprite = m_MainSystem.CAssetsPool.ObjectsPointer.ItemDictionary[Icon].ItemIcon;
                    }
                }
                break;
        }

    }

}
