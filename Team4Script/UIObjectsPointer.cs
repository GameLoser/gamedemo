﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIObjectsPointer : MonoBehaviour
{
    private MainSystem m_MainSystem = null;

    #region StaticObjectSet
    public List<Image> EquitItemButtonImage = new List<Image>();
    public Image EquitSkillButtonImage = null;
    public Text EquitTitle;
    public Image EquitImage;
    public Text EquitDepiction;
    public Text EquitAttack;
    public Text EquitVitality;

    public Toggle YInverse;
    public Image MouseSensity;
    public Image BGMVolume;
    public Image SoundVolume;
    
    public Transform SkillListPos;
    public Text SkillListTitle;
    public Text SkillListDepiction;

    public Transform ItemListPos;
    public Text ItemListTitle;
    public Image ItemListImage;
    public Text ItemListDepiction;

    public Image GamePlayItemImage;
    public Text GamePlayItemText;
    public GameObject GamePlayUse;
    public Text GamePlayMissionText;

    public Image LoadBarImage;

    public Text AreaName;
    public Image AreaImage;
    public uTools.uTweenAlpha TweenAlpha;
    #endregion

    public Image LastClickButtonImage { get; set; }

    public Dictionary<string, UIButtonPointer> ItemButtonPointer { get; private set; }
    public Dictionary<Sprite, ItemInfo> ItemDictionary { get; private set; }

    void Start ()
    {
        m_MainSystem = MainSystem.Instance;

        ItemButtonPointer = new Dictionary<string, UIButtonPointer>();
        ItemDictionary = new Dictionary<Sprite, ItemInfo>();
        LastClickButtonImage = null;

        YInverse.isOn = m_MainSystem.CRecordData.IsInverseYMove;
        MouseSensity.fillAmount = m_MainSystem.CRecordData.MouseSensity * 0.1f;
        BGMVolume.fillAmount = m_MainSystem.CRecordData.BGMVolume;
        SoundVolume.fillAmount = m_MainSystem.CRecordData.SoundVolume;

        SkillInfo info1 = new SkillInfo("奧義．天翔龍閃", m_MainSystem.CAssetsPool.ImageAssets[AssetsSprite.SKILL03], Skills.SKILL03, 1, 30,
            "飛天御劍派之極致奧義\n" +
            "在空中二段連擊後，\n" +
            "著地時再追加三連迴旋擊\n" +
            "\n" +
            "這是緋村劍心過去已習得的奧義");
        AddNewSkill(info1);
        SkillInfo info2 = new SkillInfo("雙龍天旋斬", m_MainSystem.CAssetsPool.ImageAssets[AssetsSprite.SKILL02], Skills.SKILL02, 1.2f, 30,
            "一招上位的劍術\n" +
            "因難以學習，練習時容易劃傷自己\n" +
            "\n" +
            "以二連斬作為開頭，\n" +
            "並緊接著在空旋二連擊\n" +
            "威力非同小可");
        AddNewSkill(info2);
        SkillInfo info3 = new SkillInfo("惡鬼突疾咬", m_MainSystem.CAssetsPool.ImageAssets[AssetsSprite.SKILL01], Skills.SKILL01, 1.5f, 30,
            "明治維新時代一個正義組織所使用的劍法\n" +
            "\n" +
            "是一招二連突刺加一大斬擊的劍法\n" +
            "容易穿破敵人的防禦");
        AddNewSkill(info3);

        ResetItems();

        LoadData();
    }
    /// <summary>
    /// 讀取紀錄資料
    /// </summary>
    void LoadData()
    {

        if (m_MainSystem.CRecordData.SaveItem.Count > 0)
        {
            foreach (var o in m_MainSystem.CRecordData.SaveItem.Values)
            {
                var amout = o.CurrentAmount;
                o.CurrentAmount = 0;
                if (amout > 0)
                {
                    for (int i = 0; i < amout; i++)
                        AddNewItem(o, true);
                }
                else
                    AddNewItem(o, false);
            }

            for (int i = 0; i < m_MainSystem.CRecordData.SaveEquitItem.Count; i++)
            {
                var info = m_MainSystem.CRecordData.SaveEquitItem[i];
                m_MainSystem.CGameStateInput.AddList(info, info.ItemIcon);
                EquitItemButtonImage[i].sprite = info.ItemIcon;
                if (GamePlayItemImage.sprite == m_MainSystem.CAssetsPool.ImageAssets[AssetsSprite.NULL])
                    GamePlayItemImage.sprite = info.ItemIcon;
            }
        }

        if(m_MainSystem.CRecordData.SaveEquitSkill != null)
        {
            EquitSkillButtonImage.sprite = m_MainSystem.CRecordData.SaveEquitSkill.SkillIcon;
            foreach (var o in m_MainSystem.CAssetsPool.Characters)
            {
                if (o.tag == "Player") o.SendMessage("RecieveEquitSkill", m_MainSystem.CRecordData.SaveEquitSkill);
            }
        }
        
    }

    /// <summary>
    /// 物件說明
    /// </summary>
    /// <param name="image"></param>
    public void ItemDeciption(Image image)
    {
        if(ItemDictionary.ContainsKey(image.sprite) == true)
        {
            EquitDepiction.text = ItemDictionary[image.sprite].Depiction;
            EquitTitle.text = ItemDictionary[image.sprite].ItemName;
            EquitImage.sprite = ItemDictionary[image.sprite].ItemIcon;
        }
    }
    /// <summary>
    /// 技能說明
    /// </summary>
    public void SkillDeciption()
    {
        if(m_MainSystem.CRecordData.SaveEquitSkill != null)
        {
            EquitTitle.text = m_MainSystem.CRecordData.SaveEquitSkill.SkillName;
            EquitDepiction.text = m_MainSystem.CRecordData.SaveEquitSkill.Deciption;
            EquitImage.sprite = m_MainSystem.CRecordData.SaveEquitSkill.SkillIcon;
        }
    }

    /// <summary>
    /// 新增物品或是增加數量
    /// </summary>
    /// <param name="info"></param>
    public void AddNewItem(ItemInfo info, bool isAdd)
    {
        if (ItemButtonPointer.ContainsKey(info.ItemCode))
        {
            if (ItemButtonPointer[info.ItemCode].ItemCount < info.MaxAmount && isAdd == true)
            {
                ItemButtonPointer[info.ItemCode].AddItemCount();
                ItemDictionary[info.ItemIcon].CurrentAmount++;
            }
            return;
        }
        GameObject item = Instantiate(m_MainSystem.CAssetsPool.UIAssets["ListButton"]);
        item.name = info.ItemCode;
        item.transform.SetParent(ItemListPos);
        RectTransform rt = item.GetComponent<RectTransform>();
        rt.anchorMax = new Vector2(0, 1);
        rt.anchorMin = new Vector2(0, 1);

        UIButtonPointer bp = item.GetComponent<UIButtonPointer>();
        ItemButtonPointer.Add(info.ItemCode, bp);
        bp.Init();
        bp.ObjectType = "ItemButton";
        bp.ObjectName = info.ItemName;
        bp.Icon = info.ItemIcon;
        bp.InitUI();

        ItemDictionary.Add(info.ItemIcon, info);

        Button btn = item.GetComponentInChildren<Button>();
        btn.onClick.AddListener(() => ItemButtonClick(info));

        AddNewItem(info, isAdd);
    }
    /// <summary>
    /// 道具按鈕點擊事件
    /// </summary>
    /// <param name="info"></param>
    void ItemButtonClick(ItemInfo info)
    {
        foreach (var k in EquitItemButtonImage)
        {
            if (k.sprite == info.ItemIcon)
            {
                k.sprite = m_MainSystem.CAssetsPool.ImageAssets[AssetsSprite.NULL];
                k.color = Color.white;
            }
        }

        m_MainSystem.CGameStateInput.AddList(info, LastClickButtonImage.sprite);

        Image image = LastClickButtonImage;
        image.sprite = info.ItemIcon;

        ImageColorChange(info.ItemCode, image);
        ImageColorChange(info.ItemCode, GamePlayItemImage);

        m_MainSystem.CGameStateInput.CurrentActiveWindow.SetActive(false);
        m_MainSystem.CGameStateInput.CurrentActiveWindow = m_MainSystem.CAssetsPool.UISources[Window.MAIN_MENU_WINDOW];
    }
    /// <summary>
    /// 技能增加
    /// </summary>
    /// <param name="objectName"></param>
    /// <param name="skillImage"></param>
    /// <param name="skillName"></param>
    public void AddNewSkill(SkillInfo info)
    {
        GameObject skill = Instantiate(m_MainSystem.CAssetsPool.UIAssets["ListButton"]);
        skill.transform.SetParent(SkillListPos);
        RectTransform rt = skill.GetComponent<RectTransform>();
        rt.anchorMax = new Vector2(0, 1);
        rt.anchorMin = new Vector2(0, 1);

        UIButtonPointer bp = skill.GetComponent<UIButtonPointer>();
        bp.Init();
        bp.ObjectType = "SkillButton";
        bp.ObjectName = info.SkillName;
        bp.Icon = info.SkillIcon;
        bp.SelectInfo = info;
        bp.InitUI();

        Button btn = skill.GetComponentInChildren<Button>();
        btn.onClick.AddListener(() => SkillButtonClick(info));

    }
    /// <summary>
    /// 技能按鈕點擊事件
    /// </summary>
    /// <param name="sname"></param>
    void SkillButtonClick(SkillInfo info)
    {
        Image image = LastClickButtonImage;
        image.sprite = info.SkillIcon;

        m_MainSystem.CRecordData.SaveEquitSkill = info;

        foreach (var o in m_MainSystem.CAssetsPool.Characters)
        {
            if (o.tag == "Player") o.SendMessage("RecieveEquitSkill", info);
        }
        m_MainSystem.CGameStateInput.CurrentActiveWindow.SetActive(false);
        m_MainSystem.CGameStateInput.CurrentActiveWindow = m_MainSystem.CAssetsPool.UISources[Window.MAIN_MENU_WINDOW];
    }

    public void ResetAreaWindow()
    {
        AreaImage.color = Color.white;
        AreaName.color = Color.white;
        TweenAlpha.Reset02();
    }

    public void ResetTweenAlpha(uTools.uTweenAlpha alpha)
    {
        alpha.Reset02();
        alpha.RemoveOnMiddle();
    }

    public void BossDead()
    {
        m_MainSystem.CGameStateInput.IsStopPlayerInput = true;
        m_MainSystem.CAssetsPool.UISources[Window.BLACK_FADE].SetActive(true);
        m_MainSystem.CAssetsPool.UISources[Window.BLACK_FADE].GetComponent<uTools.uTweenAlpha>().AddOnMiddle(() => BossDeadEvent());
    }

    void BossDeadEvent()
    {
        m_MainSystem.CAssetsPool.TimeLineBoss.SendMessage("TargetMovieClip", MovieClip.BossDead);
        m_MainSystem.CAssetsPool.MainCamera.gameObject.SetActive(false);
        m_MainSystem.CAssetsPool.UISources[Window.GAMEPLAY_WINDOW].SetActive(false);
        foreach (var o in MainSystem.Instance.CAssetsPool.Characters)
        {
            if (o.tag == "Player") o.transform.position = new Vector3(345f, 32f, 376f);
        }
        foreach (AIStatus status in MainSystem.Instance.CAI_System.statusList)
        {
            if (status.Rank == AI_Rank.STAGE_BOSS) status.gameObject.transform.position = Vector3.zero;
        }
    }

    public void PlayerRestart()
    {
        m_MainSystem.CAssetsPool.UISources[Window.LOAD_WINDOW].SetActive(true);
        m_MainSystem.CRecordData.SaveData(m_MainSystem.CAssetsPool.ObjectsPointer.ItemDictionary, m_MainSystem.CGameStateInput.ItemList);
        StartCoroutine(m_MainSystem.CSceneSystem.LoadScene("MainGameScene", 
            m_MainSystem.CAssetsPool.ObjectsPointer.LoadBarImage, m_MainSystem.LoadSystem));
    }

    public void ResetItems()
    {
        ItemInfo info = new ItemInfo(ItemType.HEAL, "HealDrug", m_MainSystem.CAssetsPool.ImageAssets[AssetsSprite.HEAL_DRUG], "藥丸", 10, 10, 0, 0, 0,
                "能逐漸回復HP的藥丸\n" +
                "自古便於此地流傳的祕藥\n" +
                "在遠古戰事中便有使用的紀錄，\n",
                EffectName.HEAL_WAVE1, AudioName.HEAL);

        for (int i = 0; i < info.MaxAmount; i++)
            AddNewItem(info, true);

        info = new ItemInfo(ItemType.ATTACK_UP_TEMP, "AttackDrug", m_MainSystem.CAssetsPool.ImageAssets[AssetsSprite.ATTACKSUGER], "阿攻糖", 5, 0, 2, 0, 30,
                "能獲得庇護的佛糖\n" +
                "可暫時提高攻擊力與對軀幹的攻擊力\n" +
                "只要咬碎這種糖，\n" +
                "即可使超常御靈之庇護降於己身\n" +
                "\n",
                EffectName.ATTACK_BUFF, AudioName.BUFF);

        for (int i = 0; i < info.MaxAmount; i++)
            AddNewItem(info, true);

        info = new ItemInfo(ItemType.POSTUREDEFENCE_UP_TEMP, "PostureDrug", m_MainSystem.CAssetsPool.ImageAssets[AssetsSprite.POSTURESUGER], "剛幹糖", 5, 0, 0, 2, 30,
                "能獲得庇護的佛糖\n" +
                "可暫時提高軀幹的防禦力\n" +
                "只要咬碎這種糖，\n" +
                "即可使超常御靈之庇護降於己身\n" +
                "\n",
                EffectName.POSTURE_BUFF, AudioName.BUFF);

        for (int i = 0; i < info.MaxAmount; i++)
            AddNewItem(info, true);
    }

    public void ImageColorChange(string itemCode, Image changeTarget)
    {
        if(ItemButtonPointer[itemCode].ItemCount <= 0)
            changeTarget.color = Color.gray;
        else
            changeTarget.color = Color.white;
    }

}
