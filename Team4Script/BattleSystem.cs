﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSystem
{
    private Dictionary<GameObject, CharacterStatus> m_AllStatusDictionary = new Dictionary<GameObject, CharacterStatus>();

    //public BattleSystem()
    //{
    //    foreach (var o in MainSystem.Instance.CAssetsPool.Characters)
    //    {
    //        var status = o.GetComponent<CharacterStatus>();
    //        if (status != null)
    //            m_AllStatusDictionary.Add(o, status);
    //    }
    //}

    public void SomeoneGetDamage(GameObject hitter, GameObject getHitter, Vector3 hitPos)
    {
        if (m_AllStatusDictionary.ContainsKey(getHitter) == true)
            m_AllStatusDictionary[getHitter].GetDamage(m_AllStatusDictionary[hitter].DamageInfo, hitPos);
    }

    public void SomeonePostureIncrease(GameObject hitter, GameObject getHitter)
    {
        if (m_AllStatusDictionary.ContainsKey(getHitter) == true)
            m_AllStatusDictionary[getHitter].GetIncreasePosture(m_AllStatusDictionary[hitter].DamageInfo);
    }

    public void SomeoneFatalDead(GameObject Hitter, GameObject getHitter)
    {
        if (m_AllStatusDictionary.ContainsKey(getHitter) == true)
            m_AllStatusDictionary[getHitter].PlayFatalDeadAnimator(Hitter);
    }

    public void RemoveList(GameObject obj)
    {
        MainSystem.Instance.CAssetsPool.RemoveObject(obj);
        m_AllStatusDictionary.Remove(obj);
    }

    public void ResetStatusList()
    {
        m_AllStatusDictionary.Clear();
        //Debug.Log(MainSystem.Instance.CAssetsPool.Characters.Count);
        foreach (var o in MainSystem.Instance.CAssetsPool.Characters)
        {
            var status = o.GetComponent<CharacterStatus>();
            if (status != null)
                m_AllStatusDictionary.Add(o, status);
        }
    }

}
