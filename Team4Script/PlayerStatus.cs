﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus : CharacterStatus
{
    private Animator m_Animator = null;
    private UIObjectsPointer m_ObjectsPointer = null;

    private int m_Attack = 0;
    private int m_OriginAttack = 0;
    private int m_PostureDefence = 0;
    private int m_OriginPostureDefence = 0;

    private bool m_IsUse = false;
    private bool m_IsPerfectDefence = false;

    void Start ()
    {
        m_Animator = GetComponent<Animator>();
        m_ObjectsPointer = MainSystem.Instance.CAssetsPool.ObjectsPointer;
        InitStatus();
    }

    private void Update()
    {
        if (Posture > 0 && IsInvoking("RecoverPosture") == false) InvokeRepeating("RecoverPosture", 2, 1);
    }
    /// <summary>
    /// 初始化狀態值
    /// </summary>
    private void InitStatus()
    {
        DamageInfo = new DamageInfo();
        DamageInfo.Hitter = this.gameObject;

        MaxHealthPoint = 20;
        HealthPoint = MaxHealthPoint;
        Posture = 0;
        MaxPosture = 100;
        m_Attack = 2;
        m_OriginAttack = m_Attack;
        m_PostureDefence = 5;
        m_OriginPostureDefence = m_PostureDefence;
    }

    void AnimationEvnetCanUse() { m_IsUse = false; }
    /// <summary>
    /// 計算傷害與軀幹值
    /// </summary>
    /// <param name="info"></param>
    private void CalculateDamage(SkillInfo info = null)
    {
        if(info != null)
        {
            DamageInfo.HealthDamage = Mathf.CeilToInt(m_Attack * info.Magnification);
            DamageInfo.PostureDamage = info.PostureDamage;
        }
    }
    /// <summary>
    /// 時間到定時回復軀幹值
    /// </summary>
    private void RecoverPosture()
    {
        Posture -= 5;
        MainSystem.Instance.CHUDSystem.UpdateUIStatus(this);
        if (Posture <= 0) CancelInvoke("RecoverPosture");
    }
    /// <summary>
    /// 完美防禦計算
    /// </summary>
    /// <param name="info"></param>
    /// <param name="hitPos"></param>
    /// <param name="hitDir"></param>
    /// <returns></returns>
    private bool OnDefence(DamageInfo info,Vector3 hitPos,float hitDir)
    {
        if(PlayerAnimator.CurrentAnimationState(m_Animator) == PlayerState.DEFENCE && hitDir > 0)
        {
            if(PlayerAnimator.CurrentAnimatorStateInfo(m_Animator).normalizedTime < 0.5f)
            {
                m_IsPerfectDefence = true;
                MainSystem.Instance.CBattleSystem.SomeonePostureIncrease(this.transform.gameObject, info.Hitter);
                PlayerAnimator.AnimatorPerfectDefense(m_Animator);
            }
            var playPosition = hitPos + transform.up * 1.5f + transform.forward * 0.7f;
            MainSystem.Instance.CAudioSystem.PlayAudio(AudioName.DEFLECTION1, 0.5f);
            MainSystem.Instance.CEffectSystem.PlayEffect(EffectName.CIRCLE_SPARKS2, playPosition);
            return true;
        }
        return false;
    }
    /// <summary>
    /// 使用道具
    /// </summary>
    /// <param name="info"></param>
    public void GetSomeStatusChange(ItemInfo info)
    {
        if (info == null || m_IsUse == true ||
            m_ObjectsPointer.ItemButtonPointer[info.ItemCode].ItemCount <= 0 || 
            MainSystem.Instance.CGameStateInput.CurrentActiveWindow != null ||
            (PlayerAnimator.CurrentAnimationState(m_Animator) != PlayerState.IDLE &&
            PlayerAnimator.CurrentAnimationState(m_Animator) != PlayerState.RUN &&
            PlayerAnimator.CurrentAnimationState(m_Animator) != PlayerState.LOCK_ON_RUN)) return;

        m_IsUse = true;
        if (HealthPoint + info.HealPoint < MaxHealthPoint) HealthPoint += info.HealPoint;
        else HealthPoint = MaxHealthPoint;
        MainSystem.Instance.CHUDSystem.UpdateUIStatus(this);
        MainSystem.Instance.CEffectSystem.PlayEffect(info.EffectType, 
            this.transform, Vector3.up * 1.5f, 0.7f);
        MainSystem.Instance.CAudioSystem.PlayAudio(info.AudioType, 1, 0.7f);

        m_ObjectsPointer.ItemButtonPointer[info.ItemCode].GetComponent<UIButtonPointer>().MinusItemCount();
        m_ObjectsPointer.ItemDictionary[info.ItemIcon].CurrentAmount--;

        if (info.TheItemType == ItemType.ENCHANT) PlayerAnimator.AnimatorEnchant(m_Animator);
        else PlayerAnimator.AnimatorEat(m_Animator);

        if (info.TheItemType == ItemType.HEAL) return;

        m_Attack = m_OriginAttack;
        m_PostureDefence = m_OriginPostureDefence;
        StopAllCoroutines();
        StartCoroutine(ItemTimer(info));
    }

    private IEnumerator ItemTimer(ItemInfo info)
    {
        switch (info.TheItemType)
        {
            case ItemType.ATTACK_UP_TEMP:
            case ItemType.ENCHANT:
                m_Attack += info.AddTempAttack;
                break;
            case ItemType.POSTUREDEFENCE_UP_TEMP:
                m_PostureDefence += info.AddTempPostureDefence;
                break;
        }
        yield return new WaitForSeconds(info.ItemTime);
        switch (info.TheItemType)
        {
            case ItemType.ATTACK_UP_TEMP:
            case ItemType.ENCHANT:
                m_Attack = m_OriginAttack;
                break;
            case ItemType.POSTUREDEFENCE_UP_TEMP:
                m_PostureDefence = m_OriginPostureDefence;
                break;
        }
    }

    public void ResetStatus()
    {
        HealthPoint = MaxHealthPoint;
        Posture = 0;
        m_Attack = 2;
        m_PostureDefence = 5;
        MainSystem.Instance.CHUDSystem.UpdateUIStatus(this);
    }

    public override void GetDamage(DamageInfo info, Vector3 hitPos)
    {
        var hitDirect = info.Hitter.transform.position - transform.position;
        var hitAngle = Vector3.SignedAngle(hitDirect, transform.forward, transform.up);
        var dieDir = Vector3.Dot(hitDirect, transform.forward);

        if (OnDefence(info, hitPos, dieDir) == true
            || PlayerAnimator.CurrentAnimationState(m_Animator) == PlayerState.DODGE
            || PlayerAnimator.CurrentAnimationState(m_Animator) == PlayerState.LOCK_DODGE
            || PlayerAnimator.CurrentAnimationState(m_Animator) == PlayerState.FATAL_ATTACK)
            return;

        HealthPoint -= info.HealthDamage;
        MainSystem.Instance.CHUDSystem.UpdateUIStatus(this);

        if (HealthPoint <= 0)
        {
            if (dieDir > 0) PlayerAnimator.AnimatorDead(m_Animator, true, 0);
            else PlayerAnimator.AnimatorDead(m_Animator, true, 1);
        }
        else
        {
            if(PlayerAnimator.CurrentAnimationState(m_Animator) != PlayerState.SKILLATTACK)
            {
                if (Posture < MaxPosture)
                {
                    if (hitAngle >= -45 && hitAngle <= 45)
                        PlayerAnimator.AnimatorGetHit(m_Animator, 0, true);
                    else if (hitAngle >= 135 || hitAngle <= -135)
                        PlayerAnimator.AnimatorGetHit(m_Animator, 1, true);
                    else if (hitAngle > 45 && hitAngle < 135)
                        PlayerAnimator.AnimatorGetHit(m_Animator, 2, true);
                    else if (hitAngle > -135 && hitAngle < -45)
                        PlayerAnimator.AnimatorGetHit(m_Animator, 3, true);
                }
                else
                    PlayerAnimator.AnimatorGetHit(m_Animator, 4, true);
            }
        }

        var playPosition = hitPos + transform.up * 1.5f;
        MainSystem.Instance.CAudioSystem.PlayAudio(AudioName.GET_HIT1, 0.7f);
        MainSystem.Instance.CEffectSystem.PlayEffect(EffectName.CIRCLE_SPARKS2, playPosition);

    }

    public override void GetIncreasePosture(DamageInfo info)
    {
        if (PlayerAnimator.CurrentAnimationState(m_Animator) == PlayerState.DODGE
            || PlayerAnimator.CurrentAnimationState(m_Animator) == PlayerState.LOCK_DODGE
            || PlayerAnimator.CurrentAnimationState(m_Animator) == PlayerState.FATAL_ATTACK
            || m_IsPerfectDefence == true)
        {
            //Debug.Log(m_IsPerfectDefence);
            m_IsPerfectDefence = false;
            return;
        }

        var dir = Vector3.Dot(info.Hitter.transform.position - transform.position, transform.forward);
        var value = info.PostureDamage - m_PostureDefence;

        if (PlayerAnimator.CurrentAnimationState(m_Animator) == PlayerState.DEFENCE && dir > 0)
        {
            if (Posture < MaxPosture)
                PlayerAnimator.AnimatorGuardGetHit(m_Animator, 0);
            else
            {
                PlayerAnimator.AnimatorGuardGetHit(m_Animator, 1);
                PlayerAnimator.AnimatorOutOfStamina(m_Animator);
            }
        }

        if (Posture < MaxPosture)
        {
            Posture += value > 5 ? value : 5;
            Posture = Mathf.Clamp(Posture, 0, 100);
        }
        else Posture = 0;

        MainSystem.Instance.CHUDSystem.UpdateUIStatus(this);

        if (IsInvoking("RecoverPosture") == true) CancelInvoke("RecoverPosture");
    }

}
