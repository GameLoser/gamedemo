﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimeLineDirector : MonoBehaviour
{
    private MainSystem m_MainSystem = MainSystem.Instance;

    public GameObject[] TimeLineClips = new GameObject[] { };

    private PlayableDirector m_Director = null;
    private bool m_OneCall = false;

    private MovieClip m_CurrentClip = MovieClip.None;

    void Update ()
    {
        if(m_Director != null)
        {
            if (m_Director.time >= m_Director.duration * 0.9f && m_OneCall == false)
            {
                m_MainSystem.CAssetsPool.UISources[Window.BLACK_FADE].SetActive(true);
                switch (m_CurrentClip)
                {
                    case MovieClip.BossEnter:
                        m_MainSystem.CAssetsPool.UISources[Window.BLACK_FADE].GetComponent<uTools.uTweenAlpha>().AddOnMiddle(() => BossAreaFinish());
                        break;
                    case MovieClip.BossDead:
                        m_MainSystem.CAssetsPool.UISources[Window.BLACK_FADE].GetComponent<uTools.uTweenAlpha>().AddOnMiddle(() => BossDead());
                        break;
                    case MovieClip.Open:
                        m_MainSystem.CAssetsPool.UISources[Window.BLACK_FADE].GetComponent<uTools.uTweenAlpha>().AddOnMiddle(() => OpenFinish());
                        break;
                }
                m_OneCall = true;
            }
        }
        
    }

    void TargetMovieClip(MovieClip movieClip)
    {
        m_CurrentClip = movieClip;
        m_OneCall = false;
        switch (movieClip)
        {
            case MovieClip.BossEnter:
                TimeLineClips[0].SetActive(true);
                m_Director = TimeLineClips[0].GetComponent<PlayableDirector>();
                break;
            case MovieClip.BossDead:
                TimeLineClips[1].SetActive(true);
                m_Director = TimeLineClips[1].GetComponent<PlayableDirector>();
                break;
            case MovieClip.Open:
                TimeLineClips[2].SetActive(true);
                m_Director = TimeLineClips[2].GetComponent<PlayableDirector>();
                break;
        }
    }

    void OpenFinish()
    {
        m_MainSystem.CGameStateInput.IsStopPlayerInput = false;
        m_MainSystem.CAssetsPool.MainCamera.gameObject.SetActive(true);
        TimeLineClips[2].SetActive(false);
        m_MainSystem.CRecordData.SavePostion = new Vector3(262, 33, 53);
        m_MainSystem.CAssetsPool.UISources[Window.GAMEPLAY_WINDOW].SetActive(true);
        foreach (var o in m_MainSystem.CAssetsPool.Characters)
        {
            if (o.tag == "Player") o.transform.position = m_MainSystem.CRecordData.SavePostion;
        }
        m_MainSystem.EGameState = GameState.GAMEPLAY;
        m_MainSystem.IsOneCall = false;
        m_Director = null;
    }

    void BossAreaFinish()
    {
        m_MainSystem.CGameStateInput.IsStopPlayerInput = false;
        m_MainSystem.CAssetsPool.MainCamera.gameObject.SetActive(true);
        TimeLineClips[0].SetActive(false);
        m_MainSystem.CAssetsPool.UISources[Window.GAMEPLAY_WINDOW].SetActive(true);
        foreach (var o in m_MainSystem.CAssetsPool.Characters)
        {
            if (o.tag == "Player") o.transform.position = new Vector3(332, 30, 412);
        }
        m_Director = null;
    }

    void BossDead()
    {
        StartCoroutine(m_MainSystem.CSceneSystem.LoadScene("WelcomeScene",
            m_MainSystem.CAssetsPool.ObjectsPointer.LoadBarImage,
            m_MainSystem.WelcomeSceneSet));
    }

}

public enum MovieClip
{
    None,
    BossEnter,
    BossDead,
    Open
}