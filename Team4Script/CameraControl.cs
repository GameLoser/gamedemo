﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    private CharacterStatus m_CharacterStatus = null;

    //目標
    public Transform TargetLookAt { get; private set; }
    //攝影機距離目標
    [SerializeField]
    private float m_CameraDistance = 2.5f;
    [SerializeField]
    private float m_LockDistance = 10f;

    //private float m_XRotateMoveSmooth = 0.05f;
    //private float m_YRotateMoveSmooth = 0.1f;

    private float m_VelX = 0;
    private float m_VelY = 0;
    private float m_VelZ = 0;
    private float m_velDistance = 0;
    private float m_OriginDistance = 0;
    private float m_DesiredDistance = 0;
    private Vector3 m_Position = Vector3.zero;
    private Vector3 m_DesiredPosition = Vector3.zero;

    private Vector3 m_ViewPos = Vector3.zero;
    //private Transform m_NearestEnemy;

    //X靈敏度
    public float MouseSensitivityX { get; set; }
    //Y靈敏度
    public float MouseSensitivityY { get; set; }
    //Y軸轉動角度最小值
    public float YAngleMin { get; private set; }
    //Y軸轉動角度最大值
    public float YAngleMax { get; private set; }
    //攝影機Y軸反向操作
    public bool YInverse { get; set; }
    public float MouseX { get; set; }
    public float MouseY { get; set; }
    public float ChangeLock { get; set; }
    public bool IsLockOn{ get; private set; }
    

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        TargetLookAt = transform.parent;
        foreach (var o in MainSystem.Instance.CAssetsPool.Characters)
        {
            if (o.tag == "Player") m_CharacterStatus = o.GetComponent<CharacterStatus>();
        }
        if (TargetLookAt == null)
            return;

        CameraReset();
        MouseX = 168;
        MouseY = 10;
    }

    void LateUpdate()
    {
        if (TargetLookAt == null)
            return;

        CameraInputUpdate();

        CalcualateDesiredPosition();
        CheckIfOccluded();

        UpdatePosition();

    }

    /// <summary>
    /// 滑鼠輸入控制攝影機
    /// </summary>
    public void CameraInputUpdate()
    {
        if(MainSystem.Instance.CGameStateInput.IsStopPlayerInput == false)
        {
            if (IsLockOn == false)
            {
                MouseX += Input.GetAxis("Mouse X") * MouseSensitivityX;
                MouseY += Input.GetAxis("Mouse Y") * MouseSensitivityY * (YInverse ? -1 : 1);

                MouseY = Mathf.Clamp(MouseY, YAngleMin, YAngleMax);
            }
            else
            {
                ChangeLock += Input.GetAxis("Mouse X");
                if (ChangeLock >= 50)
                    ChangeOrLock(true, MouseMoveSide.LEFT);
                else if (ChangeLock <= -50)
                    ChangeOrLock(true, MouseMoveSide.RIGHT);
            }
        }
        
    }

    void CalcualateDesiredPosition()
    {
        ResetDesiredDistance();
        m_CameraDistance = Mathf.SmoothDamp(m_CameraDistance, m_DesiredDistance, ref m_velDistance, 0.1f);
        //
        m_DesiredPosition = CalcualatePosition(MouseY, MouseX, m_CameraDistance);
    }
    /// <summary>
    /// 計算移動的位置
    /// </summary>
    /// <param name="rotationX">X軸</param>
    /// <param name="rotationY">Y軸</param>
    /// <param name="distance">距離</param>
    /// <returns></returns>
    Vector3 CalcualatePosition(float rotationX, float rotationY, float distance)
    {
        Vector3 lookDistance = new Vector3(0, 0, -distance);
        Quaternion camRotation;
        if (IsLockOn == true)
        {
            Vector3 dir = TargetLookAt.position - (transform.position - Vector3.up);
            dir.Normalize();
            if (dir == Vector3.zero)
                dir = transform.forward;

            camRotation = Quaternion.LookRotation(dir);
            MouseY = transform.eulerAngles.x - transform.eulerAngles.x < 0 ? 360 : 0;
            MouseX = transform.localEulerAngles.y;
        }
        else
            camRotation = Quaternion.Euler(rotationX, rotationY, 0);
            
        return transform.parent.position + camRotation * lookDistance;
    }
    /// <summary>
    /// 更新攝影機位置
    /// </summary>
    void UpdatePosition()
    {
        var posX = Mathf.SmoothDamp(m_Position.x, m_DesiredPosition.x, ref m_VelX, 0.05f);
        var posY = Mathf.SmoothDamp(m_Position.y, m_DesiredPosition.y, ref m_VelY, 0.1f);
        var posZ = Mathf.SmoothDamp(m_Position.z, m_DesiredPosition.z, ref m_VelZ, 0.05f);
        m_Position = new Vector3(posX, posY, posZ);

        transform.position = m_Position;
        transform.LookAt(IsLockOn ? TargetLookAt.position + Vector3.up : TargetLookAt.position);
    }

    void CheckIfOccluded()
    {
        var nearestDistance = CheckCameraPoints(transform.parent.position, m_DesiredPosition);

        if (nearestDistance != -1)
        {
            if (nearestDistance < 0.3f)
                m_DesiredDistance = 0.3f;
            else
                m_DesiredDistance = nearestDistance - MainSystem.Instance.CAssetsPool.MainCamera.nearClipPlane;
        }

    }

    void ResetDesiredDistance()
    {
        if (m_DesiredDistance < m_OriginDistance)
        {
            var pos = CalcualatePosition(MouseY, MouseX, m_OriginDistance);
            var nearestDistance = CheckCameraPoints(transform.parent.position , pos);
            if (nearestDistance == -1 || nearestDistance > m_OriginDistance)
                m_DesiredDistance = m_OriginDistance;
        }
    }

    float CheckCameraPoints(Vector3 from, Vector3 to)
    {
        var nearestDistance = -1f;

        RaycastHit hitInfo;

        Helper.ClipPlanePoints clipPlanePoints = Helper.ClipPlaneAtNear(to);

        if (Physics.Linecast(from, clipPlanePoints.UpperLeft, out hitInfo) && hitInfo.collider.tag != "Player")
        {
            if (hitInfo.distance < nearestDistance || nearestDistance == -1)
            {
                nearestDistance = hitInfo.distance;
            }
        }
        if (Physics.Linecast(from, clipPlanePoints.LowerLeft, out hitInfo) && hitInfo.collider.tag != "Player")
        {
            if (hitInfo.distance < nearestDistance || nearestDistance == -1)
            {
                nearestDistance = hitInfo.distance;
            }
        }

        if (Physics.Linecast(from, clipPlanePoints.UpperRight, out hitInfo) && hitInfo.collider.tag != "Player")
        {
            if (hitInfo.distance < nearestDistance || nearestDistance == -1)
            {
                nearestDistance = hitInfo.distance;
            }
        }

        if (Physics.Linecast(from, clipPlanePoints.LowerRight, out hitInfo) && hitInfo.collider.tag != "Player")
        {
            if (hitInfo.distance < nearestDistance || nearestDistance == -1)
            {
                nearestDistance = hitInfo.distance;
            }
        }

        if (Physics.Linecast(from, to + transform.forward * -MainSystem.Instance.CAssetsPool.MainCamera.nearClipPlane, out hitInfo) && hitInfo.collider.tag != "Player")
        {
            if (hitInfo.distance < nearestDistance || nearestDistance == -1)
            {
                nearestDistance = hitInfo.distance;
            }
        }

        return nearestDistance;
    }

    void CameraReset()
    {
        MouseX = 0;
        MouseY = 10;
        MouseSensitivityX = MainSystem.Instance.CRecordData.MouseSensity;
        MouseSensitivityY = MainSystem.Instance.CRecordData.MouseSensity;
        YAngleMin = -55;
        YAngleMax = 60;
        YInverse = MainSystem.Instance.CRecordData.IsInverseYMove;
        ChangeLock = 0;
        m_OriginDistance = m_CameraDistance;
    }
    /// <summary>
    /// 鎖定敵人
    /// </summary>
    public void LockOnEnemy()
    {
        if (IsLockOn == false)
        {
            //MainSystem.Instance.CHUDSystem.UpdateFocusSwitch(true, m_CharacterStatus);
            ChangeOrLock(false);
        }
        else
        {
            //MainSystem.Instance.CHUDSystem.UpdateFocusSwitch(false, m_CharacterStatus);
            UnlockEnemy();
        }
    }
    /// <summary>
    /// 鎖定或是轉換鎖定
    /// </summary>
    /// <param name="isChage">確定轉換鎖定</param>
    /// <param name="mms">往左或右轉換</param>
    public void ChangeOrLock(bool isChage, MouseMoveSide mms = MouseMoveSide.NONE)
    {
        var Enemies = MainSystem.Instance.CAssetsPool.Characters;
        Transform m_NearestEnemy = null;
        for (int i = 0; i < Enemies.Count; i++)
        {
            if (Enemies[i].tag == "Enemy" && Vector3.Distance(transform.position, Enemies[i].transform.position) < m_LockDistance)//鎖定距離內
            {
                m_ViewPos = MainSystem.Instance.CAssetsPool.MainCamera.WorldToViewportPoint(Enemies[i].transform.position);
                if (m_ViewPos.x >= -0.1f && m_ViewPos.x <= 1.1f && m_ViewPos.y >= -0.1f && m_ViewPos.y <= 1.1f && m_ViewPos.z > 0)//視線內
                {
                    if (m_NearestEnemy == null) m_NearestEnemy = Enemies[i].transform;
                    if (isChage == true)
                    {
                        if (mms == MouseMoveSide.RIGHT)
                        {
                            if (Vector3.SignedAngle(Enemies[i].transform.position - transform.position, transform.forward, Vector3.up) >
                                Vector3.SignedAngle(m_NearestEnemy.position - transform.position, transform.forward, Vector3.up))
                                m_NearestEnemy = Enemies[i].transform;
                        }
                        else if (mms == MouseMoveSide.LEFT)
                        {
                            if (Vector3.SignedAngle(Enemies[i].transform.position - transform.position, transform.forward, Vector3.up) <
                                Vector3.SignedAngle(m_NearestEnemy.position - transform.position, transform.forward, Vector3.up))
                                m_NearestEnemy = Enemies[i].transform;
                        }
                    }
                    else
                    {
                        //與攝影機照射正前方夾角最小者
                        if (Vector3.Angle(Enemies[i].transform.position - transform.position, transform.forward) < Vector3.Angle(m_NearestEnemy.position - transform.position, transform.forward))
                            m_NearestEnemy = Enemies[i].transform;
                    }

                }
            }

        }
        if (m_NearestEnemy != null)
        {
            TargetLookAt = m_NearestEnemy;
            //MainSystem.Instance.CHUDSystem.UpdateUIStatus(m_CharacterStatus);
 
            //MainSystem.Instance.CHUDSystem.UpdateCameraFocus(TargetLookAt,m_CharacterStatus);
            //Debug.Log("Hi2");
            //Debug.Log("TargetLookAtTag: " + TargetLookAt.tag);
            
            IsLockOn = true;
            ChangeLock = 0;
        }
        else
            UnlockEnemy();

        
    }
    /// <summary>
    /// 取消鎖定
    /// </summary>
    public void UnlockEnemy()
    {
        IsLockOn = false;
        ChangeLock = 0;
        TargetLookAt = transform.parent;
    }


}
