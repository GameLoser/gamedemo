﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator
{
    public static PlayerState CurrentAnimationState(Animator animator)
    {
        if (animator == null) return PlayerState.NONE;
        AnimatorStateInfo animatorStateInfo = animator.GetCurrentAnimatorStateInfo(0);

        if (animatorStateInfo.tagHash == Animator.StringToHash("Idle"))
            return PlayerState.IDLE;
        else if (animatorStateInfo.tagHash == Animator.StringToHash("Run"))
            return PlayerState.RUN;
        else if(animatorStateInfo.tagHash == Animator.StringToHash("Dodge"))
            return PlayerState.DODGE;
        else if (animatorStateInfo.tagHash == Animator.StringToHash("SprintToStop"))
            return PlayerState.SPRINT_TO_STOP;
        else if (animatorStateInfo.tagHash == Animator.StringToHash("Attack"))
            return PlayerState.ATTACK;
        else if (animatorStateInfo.tagHash == Animator.StringToHash("SkillAttack"))
            return PlayerState.SKILLATTACK;
        else if (animatorStateInfo.tagHash == Animator.StringToHash("FatalAttack"))
            return PlayerState.FATAL_ATTACK;
        else if (animatorStateInfo.tagHash == Animator.StringToHash("Defense"))
            return PlayerState.DEFENCE;
        else if (animatorStateInfo.tagHash == Animator.StringToHash("LockDodge"))
            return PlayerState.LOCK_DODGE;
        else if (animatorStateInfo.tagHash == Animator.StringToHash("Dead"))
            return PlayerState.DEAD;
        else if (animatorStateInfo.tagHash == Animator.StringToHash("GetHit"))
            return PlayerState.GETHIT;
        else if (animatorStateInfo.tagHash == Animator.StringToHash("GuardGetHit"))
            return PlayerState.GUARDGETHIT;
        else if (animatorStateInfo.tagHash == Animator.StringToHash("OutOfStamina"))
            return PlayerState.OUT_OF_STAMINA;
        else if (animatorStateInfo.tagHash == Animator.StringToHash("Use"))
            return PlayerState.USE;
        else if (animatorStateInfo.tagHash == Animator.StringToHash("LockOnRunState"))
            return PlayerState.LOCK_ON_RUN;
        else if (animatorStateInfo.tagHash == Animator.StringToHash("Rest"))
            return PlayerState.REST;

        return PlayerState.NONE;
    }

    public static PlayerState CurrentAnimationStateLayer2(Animator animator)
    {
        if (animator == null) return PlayerState.NONE;
        AnimatorStateInfo animatorStateInfo = animator.GetCurrentAnimatorStateInfo(1);

        if (animatorStateInfo.tagHash == Animator.StringToHash("Walk"))
            return PlayerState.WALK;
        return PlayerState.NONE;
    }

    public static AnimatorStateInfo CurrentAnimatorStateInfo(Animator animator)
    {
        AnimatorStateInfo animatorStateInfo = animator.GetCurrentAnimatorStateInfo(0);
        return animatorStateInfo;
    }

    public static void AnimatorMove(Animator animator,float moveValue)
    {
        if (animator == null) return;

        animator.SetFloat("Walk", moveValue);
    }

    public static void AnimatorSprintDodge(Animator animator, bool isSprint,bool isDodge)
    {
        if (animator == null) return;

        animator.SetBool("IsSprint", isSprint);
        animator.SetBool("IsDodge", isDodge);
        if (CurrentAnimationState(animator) == PlayerState.ATTACK && isDodge == true)
            animator.ResetTrigger("Attack");
    }

    public static void AnimatorAttack(Animator animator)
    {
        if (animator == null) return;

        animator.SetTrigger("Attack");
        animator.ResetTrigger("PerfectDefence");
    }

    public static void AnimatorFetalAttack(Animator animator, int direct)
    {
        if (animator == null) return;

        animator.SetTrigger("FatalAttack");
        animator.SetInteger("FatalDirect", direct);
    }

    public static bool AnimatorCancelFetal(Animator animator, CharacterStatus character)
    {
        if (animator == null || character == null) return true;

        if(character.HealthPoint <= 0 && character.Rank != AI_Rank.MIDDLE_BOSS && character.Rank != AI_Rank.STAGE_BOSS)
        {
            animator.ResetTrigger("FatalAttack");
            return false;
        }
        //else if(character.HealthPoint <= 0 && (character.Rank == AI_Rank.MIDDLE_BOSS || character.Rank == AI_Rank.STAGE_BOSS))
        //    return !character.FatalKill;

        return true;
    }

    public static void AnimatorDefense(Animator animator, bool isDefense)
    {
        if (animator == null) return;

        animator.SetBool("IsDefense", isDefense);
        if(CurrentAnimationState(animator) == PlayerState.ATTACK && isDefense == true)
            animator.ResetTrigger("Attack");
        if (CurrentAnimationState(animator) != PlayerState.DEFENCE) animator.SetBool("IsMoveWhenDefense", false);
    }

    public static void AnimatorPerfectDefense(Animator animator)
    {
        if (animator == null) return;

        animator.SetTrigger("PerfectDefence");
        
        animator.SetFloat("PerfectType", Random.Range(0,2));
        if (CurrentAnimationState(animator) == PlayerState.SKILLATTACK)
            animator.ResetTrigger("PerfectDefence");
    }

    public static void AnimatorDefenseMove(Animator animator, bool isMove)
    {
        if (animator == null) return;

        animator.SetBool("IsMoveWhenDefense", isMove);
    }

    public static void AnimatorIsLock(Animator animator, bool isLock)
    {
        if (animator == null) return;

        animator.SetBool("IsLockOn", isLock);
    }

    public static void AnimatorLockMove(Animator animator,float moveX,float moveZ)
    {
        if (animator == null) return;

        animator.SetFloat("LockMoveX", moveX);
        animator.SetFloat("LockMoveZ", moveZ);
    }

    public static void AnimatorGetHit(Animator animator, int direct, bool isGetHit)
    {
        if (animator == null || CurrentAnimationState(animator) == PlayerState.SKILLATTACK) return;

        animator.SetBool("IsGetHit", isGetHit);
        animator.SetFloat("GetHitAndDieDirect", direct);
    }

    public static void AnimatorDead(Animator animator, bool isDead, int direct)
    {
        if (animator == null) return;

        animator.SetBool("IsDead", isDead);
        animator.SetFloat("GetHitAndDieDirect", direct);
    }

    public static void AnimatorOutOfStamina(Animator animator)
    {
        if (animator == null) return;

        animator.SetTrigger("PostureFull");
    }

    public static void AnimatorGuardGetHit(Animator animator, int state)
    {
        if (animator == null) return;

        animator.SetTrigger("GuardGetHit");
        animator.SetFloat("GetHitAndDieDirect", state);
    }

    public static void AnimatorSkill(Animator animator, int state)
    {
        if (animator == null) return;

        animator.SetInteger("SkillType", state);
        animator.ResetTrigger("PerfectDefence");
    }

    public static void AnimatorEat(Animator animator)
    {
        if (animator == null) return;

        animator.SetTrigger("Eat");
    }

    public static void AnimatorEnchant(Animator animator)
    {
        if (animator == null) return;

        animator.SetTrigger("Enchant");
    }

    public static void AnimatorRest(Animator animator)
    {
        if (animator == null || CurrentAnimationState(animator) == PlayerState.REST) return;

        animator.SetTrigger("Rest");
    }


}
