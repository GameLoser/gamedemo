﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public class AssetsPool
{
    public Dictionary<string, GameObject> UIAssets { get; private set; }
    public Dictionary<AssetsSprite, Sprite> ImageAssets { get; private set; }
    public Dictionary<Window, GameObject> UISources { get; private set; }
    public Dictionary<HUD_Name, GameObject> HUDAssets { get; set; }
    public Dictionary<Transform, HUD_Performance> HUDSources { get; set; }
    public Dictionary<AudioName, AudioClip> AudioAssets { get; private set; }
    public Dictionary<EffectName, GameObject> EffectAssets { get; private set; }
    public Dictionary<string, GameObject> CharactersAssets { get; private set; }

    //public Dictionary<HUD_Objects,Sprite> HUD_Assets { get; set; }



    public GameObject AudioPlayerAsset { get; private set; }

    public List<GameObject> Characters { get; private set; }
    public Queue<AudioPlayer> AudioPlayers { get; private set; }
    public Dictionary<EffectName, Queue<EffectPlayer>> EffectPlayerQueues { get; private set; }
    public Queue<EffectPlayer> EffectPlayerQueue1;
    public Queue<EffectPlayer> EffectPlayerQueue2;
    public Queue<EffectPlayer> EffectPlayerQueue3;
    public Queue<EffectPlayer> EffectPlayerQueue4;
    public Queue<EffectPlayer> EffectPlayerQueue5;
    public Queue<EffectPlayer> EffectPlayerQueue6;
    public Queue<EffectPlayer> EffectPlayerQueue7;
    public Queue<EffectPlayer> EffectPlayerQueue8;
    public GameObject AI_System { get; set; }
    public GameObject AudioSystem { get; set; }
    public GameObject EffectSystem { get; set; }

    public Camera MainCamera { get; set; }
    public CameraControl MainCameraControl { get; set; }

    public UIObjectsPointer ObjectsPointer { get; set; }
    public GameObject TimeLineBoss { get; set; }
    

    public AssetsPool()
    {
        
        CharactersAssets = new Dictionary<string, GameObject>();
        Characters = new List<GameObject>();
        UIAssets = new Dictionary<string, GameObject>();
        UISources = new Dictionary<Window, GameObject>();
        ImageAssets = new Dictionary<AssetsSprite, Sprite>();
        AudioAssets = new Dictionary<AudioName, AudioClip>();
        EffectAssets = new Dictionary<EffectName, GameObject>();
        HUDAssets = new Dictionary<HUD_Name, GameObject>();
        HUDSources = new Dictionary<Transform, HUD_Performance>();
        //HUD_Assets = new Dictionary<HUD_Objects, Sprite>();
        AudioPlayers = new Queue<AudioPlayer>();
        EffectPlayerQueues = new Dictionary<EffectName, Queue<EffectPlayer>>();
        EffectPlayerQueue1 = new Queue<EffectPlayer>();
        EffectPlayerQueue2 = new Queue<EffectPlayer>();
        EffectPlayerQueue3 = new Queue<EffectPlayer>();
        EffectPlayerQueue4 = new Queue<EffectPlayer>();
        EffectPlayerQueue5 = new Queue<EffectPlayer>();
        EffectPlayerQueue6 = new Queue<EffectPlayer>();
        EffectPlayerQueue7 = new Queue<EffectPlayer>();
        EffectPlayerQueue8 = new Queue<EffectPlayer>();
        CharactersAssets.Add("MainPlayer", Resources.Load<GameObject>("Prefabs/MainPlayer/MainPlayer"));
        NpcLoad();

        AI_System = Resources.Load<GameObject>("Prefabs/AI/AI_System");

        UIAssets.Add("MainWindow", Resources.Load<GameObject>("Prefabs/UI/MainCanvas"));
        UIAssets.Add("ListButton", Resources.Load<GameObject>("Prefabs/UI/ListButton"));
        HUDAssets.Add(HUD_Name.HUD_CANVAS, Resources.Load<GameObject>("Prefabs/UI/HUD_Canvas"));
        HUDAssets.Add(HUD_Name.HUD_MAIN_PLAYER, Resources.Load<GameObject>("Prefabs/UI/HUD_MainPlayer"));
        HUDAssets.Add(HUD_Name.HUD_ENEMY, Resources.Load<GameObject>("Prefabs/UI/HUD_Enemy"));
        //HUDAssets.Add(HUD_Name.HUD_BOSS, Resources.Load<GameObject>("Prefabs/UI/HUD_Boss"));

        LoadSprite();
        //LoadHUDSprite();
        AudioLoad();
        EffectLoad();
        AudioSystem = Resources.Load<GameObject>("Prefabs/Audio/AudioSystem");
        EffectSystem = Resources.Load<GameObject>("Prefabs/Effect/EffectSystem");
    }

    /// <summary>
    /// 載入NPC
    /// </summary>
    void NpcLoad()
    {
        CharactersAssets.Add("Boss", Resources.Load<GameObject>("Prefabs/NPC/Boss"));
        CharactersAssets.Add("Guard_A", Resources.Load<GameObject>("Prefabs/NPC/Guard_A"));
        CharactersAssets.Add("Guard_B", Resources.Load<GameObject>("Prefabs/NPC/Guard_B"));
        CharactersAssets.Add("Thief_A", Resources.Load<GameObject>("Prefabs/NPC/Thief_A"));
        CharactersAssets.Add("Thief_B", Resources.Load<GameObject>("Prefabs/NPC/Thief_B"));
    }

    /// <summary>
    /// 載入音效
    /// </summary>
    void AudioLoad()
    {
        //AudioPlayer
        AudioPlayerAsset = Resources.Load<GameObject>("Prefabs/Audio/AudioPlayer");
        //Sound effect
        AudioAssets.Add(AudioName.BLOCK1, Resources.Load<AudioClip>("AudioClip/SoundEffect/Block1"));
        AudioAssets.Add(AudioName.BLOCK2, Resources.Load<AudioClip>("AudioClip/SoundEffect/Block2"));
        AudioAssets.Add(AudioName.BLOCK3, Resources.Load<AudioClip>("AudioClip/SoundEffect/Block3"));
        AudioAssets.Add(AudioName.BLOCK4, Resources.Load<AudioClip>("AudioClip/SoundEffect/Block4"));
        AudioAssets.Add(AudioName.DEFLECTION1, Resources.Load<AudioClip>("AudioClip/SoundEffect/Deflection1"));
        AudioAssets.Add(AudioName.DEFLECTION2, Resources.Load<AudioClip>("AudioClip/SoundEffect/Deflection2"));
        AudioAssets.Add(AudioName.DEFLECTION3, Resources.Load<AudioClip>("AudioClip/SoundEffect/Deflection3"));
        AudioAssets.Add(AudioName.DEFLECTION4, Resources.Load<AudioClip>("AudioClip/SoundEffect/Deflection4"));
        AudioAssets.Add(AudioName.DEFLECTION5, Resources.Load<AudioClip>("AudioClip/SoundEffect/Deflection5"));
        AudioAssets.Add(AudioName.DEFLECTION6, Resources.Load<AudioClip>("AudioClip/SoundEffect/Deflection6"));
        AudioAssets.Add(AudioName.DEFLECTION7, Resources.Load<AudioClip>("AudioClip/SoundEffect/Deflection7"));
        AudioAssets.Add(AudioName.DEFLECTION8, Resources.Load<AudioClip>("AudioClip/SoundEffect/Deflection8"));
        AudioAssets.Add(AudioName.DEFLECTION9, Resources.Load <AudioClip>("AudioClip/SoundEffect/Deflection9"));
        AudioAssets.Add(AudioName.DEFLECTION10, Resources.Load<AudioClip>("AudioClip/SoundEffect/Deflection10"));
        AudioAssets.Add(AudioName.GET_HIT1, Resources.Load<AudioClip>("AudioClip/SoundEffect/GetHit1"));
        AudioAssets.Add(AudioName.GET_HIT2, Resources.Load<AudioClip>("AudioClip/SoundEffect/GetHit2"));
        AudioAssets.Add(AudioName.ALERT, Resources.Load<AudioClip>("AudioClip/SoundEffect/Alert"));
        AudioAssets.Add(AudioName.ALERTED, Resources.Load<AudioClip>("AudioClip/SoundEffect/Alerted"));
        AudioAssets.Add(AudioName.HEAL, Resources.Load<AudioClip>("AudioClip/SoundEffect/Heal"));
        AudioAssets.Add(AudioName.BUFF, Resources.Load<AudioClip>("AudioClip/SoundEffect/Buff"));
        AudioAssets.Add(AudioName.MAP_MESSAGE, Resources.Load<AudioClip>("AudioClip/SoundEffect/MapMessage"));
        AudioAssets.Add(AudioName.CAN_FATAL_KILL, Resources.Load<AudioClip>("AudioClip/SoundEffect/CanFatalKill"));
        AudioAssets.Add(AudioName.CAN_FATAL_KILL2, Resources.Load<AudioClip>("AudioClip/SoundEffect/CanFatalKill2"));
        AudioAssets.Add(AudioName.FATAL_KILL, Resources.Load<AudioClip>("AudioClip/SoundEffect/FatalKill"));
        AudioAssets.Add(AudioName.EXECUTION, Resources.Load<AudioClip>("AudioClip/SoundEffect/Execution"));
        AudioAssets.Add(AudioName.SNEAK_SCENE, Resources.Load<AudioClip>("AudioClip/BGM/SneakScene"));
        AudioAssets.Add(AudioName.ALERT_SCENE, Resources.Load<AudioClip>("AudioClip/BGM/AlertScene"));
        AudioAssets.Add(AudioName.FIGHT_SCENE, Resources.Load<AudioClip>("AudioClip/BGM/FightScene"));
        AudioAssets.Add(AudioName.MIDDLE_BOSS_SCENE, Resources.Load<AudioClip>("AudioClip/BGM/MiddleBossScene"));
        AudioAssets.Add(AudioName.STAGE_BOSS_SCENE, Resources.Load<AudioClip>("AudioClip/BGM/StageBossScene"));
    }

    /// <summary>
    /// 載入特效
    /// </summary>
    void EffectLoad()
    {
        EffectAssets.Add(EffectName.SPARKS1, Resources.Load<GameObject>("Prefabs/Effect/Sparks1"));
        EffectAssets.Add(EffectName.SPARKS2, Resources.Load<GameObject>("Prefabs/Effect/Sparks2"));
        EffectAssets.Add(EffectName.CIRCLE_SPARKS1, Resources.Load<GameObject>("Prefabs/Effect/CircleSparks1"));
        EffectAssets.Add(EffectName.CIRCLE_SPARKS2, Resources.Load<GameObject>("Prefabs/Effect/CircleSparks2"));
        EffectAssets.Add(EffectName.HEAL_WAVE1, Resources.Load<GameObject>("Prefabs/Effect/HealWave1"));
        EffectAssets.Add(EffectName.ATTACK_BUFF, Resources.Load<GameObject>("Prefabs/Effect/AttackBuff"));
        EffectAssets.Add(EffectName.POSTURE_BUFF, Resources.Load<GameObject>("Prefabs/Effect/PostureBuff"));
        EffectAssets.Add(EffectName.SAVE_WAVE, Resources.Load<GameObject>("Prefabs/Effect/SaveWave"));
    }

    /// <summary>
    /// 載入圖像
    /// </summary>
    void LoadSprite()
    {
        ImageAssets.Add(AssetsSprite.NULL, Resources.Load<Sprite>("Images/UIMenu/Alpha"));
        ImageAssets.Add(AssetsSprite.TAB_NORMAL, Resources.Load<Sprite>("Images/UIMenu/TabNormal"));
        ImageAssets.Add(AssetsSprite.TAB_HIGHLIGHT, Resources.Load<Sprite>("Images/UIMenu/TabHighLight"));
        ImageAssets.Add(AssetsSprite.SKILL01, Resources.Load<Sprite>("Images/UIMenu/Skill01"));
        ImageAssets.Add(AssetsSprite.SKILL02, Resources.Load<Sprite>("Images/UIMenu/Skill02"));
        ImageAssets.Add(AssetsSprite.SKILL03, Resources.Load<Sprite>("Images/UIMenu/Skill03"));
        ImageAssets.Add(AssetsSprite.HEAL_BOTTLE, Resources.Load<Sprite>("Images/UIMenu/HealBottle"));
        ImageAssets.Add(AssetsSprite.HEAL_DRUG, Resources.Load<Sprite>("Images/UIMenu/HealDrug"));
        ImageAssets.Add(AssetsSprite.ATTACKSUGER, Resources.Load<Sprite>("Images/UIMenu/AttackSuger"));
        ImageAssets.Add(AssetsSprite.POSTURESUGER, Resources.Load<Sprite>("Images/UIMenu/PostureSuger"));
        ImageAssets.Add(AssetsSprite.ITEM_BG, Resources.Load<Sprite>("Images/UIMenu/ItemBG"));
        ImageAssets.Add(AssetsSprite.SKILL_BG, Resources.Load<Sprite>("Images/UIMenu/SkillBG"));
    }
    /// <summary>
    /// 載入 HUD 圖像
    /// </summary>
    /// 
    /*void LoadHUDSprite()
    {
        HUD_Assets.Add(HUD_Objects.HEALTH, Resources.Load<Sprite>("Images/HUD/Enemy/Health"));
        HUD_Assets.Add(HUD_Objects.HEALTH_SUBBG, Resources.Load<Sprite>("Images/HUD/Enemy/Health_SubBg"));
        HUD_Assets.Add(HUD_Objects.HEALTH_BG, Resources.Load<Sprite>("Images/HUD/Enemy/Health_Bg"));
        HUD_Assets.Add(HUD_Objects.POSTURE_LEFT, Resources.Load<Sprite>("Images/HUD/Enemy/Posture"));
        HUD_Assets.Add(HUD_Objects.POSTURE_RIGHT, Resources.Load<Sprite>("Images/HUD/Enemy/Posture"));
        HUD_Assets.Add(HUD_Objects.POSTURE_BG, Resources.Load<Sprite>("Images/HUD/Enemy/Posture_Bg"));
        HUD_Assets.Add(HUD_Objects.POSTURE_CENTER, Resources.Load<Sprite>("Images/HUD/Enemy/Posture_Center"));
        HUD_Assets.Add(HUD_Objects.DETECTINDI_WHITE, Resources.Load<Sprite>("Images/HUD/Enemy/Detect"));
        HUD_Assets.Add(HUD_Objects.DETECTINDI_YELLOW, Resources.Load<Sprite>("Images/HUD/Enemy/Detect"));
        HUD_Assets.Add(HUD_Objects.DETECT_RED, Resources.Load<Sprite>("Images/HUD/Enemy/Detect"));
        HUD_Assets.Add(HUD_Objects.DETECTINDI_WHITE, Resources.Load<Sprite>("Images/HUD/Enemy/DetectIndicator"));
        HUD_Assets.Add(HUD_Objects.DETECTINDI_YELLOW, Resources.Load<Sprite>("Images/HUD/Enemy/DetectIndicator"));
        HUD_Assets.Add(HUD_Objects.DETECTINDI_RED, Resources.Load<Sprite>("Images/HUD/Enemy/DetectIndicator"));
        HUD_Assets.Add(HUD_Objects.FOCUS, Resources.Load<Sprite>("Images/HUD/Enemy/Focus"));
        HUD_Assets.Add(HUD_Objects.FATALKILL, Resources.Load<Sprite>("Images/HUD/Enemy/FatalKill"));
        HUD_Assets.Add(HUD_Objects.BOSS_HEALTH, Resources.Load<Sprite>("Images/HUD/Enemy/Health"));
        HUD_Assets.Add(HUD_Objects.BOSS_HEALTH_SUBBG, Resources.Load<Sprite>("Images/HUD/Enemy/Health_SubBg"));
        HUD_Assets.Add(HUD_Objects.BOSS_HEALTH_BG, Resources.Load<Sprite>("Images/HUD/Enemy/Health_Bg"));
        HUD_Assets.Add(HUD_Objects.BOSS_LEFTPOSTURE, Resources.Load<Sprite>("Images/HUD/Enemy/Posture"));
        HUD_Assets.Add(HUD_Objects.BOSS_LEFTPOSTURE, Resources.Load<Sprite>("Images/HUD/Enemy/Posture"));
        HUD_Assets.Add(HUD_Objects.PLAYER_HEALTH, Resources.Load<Sprite>("Images/HUD/Enemy/Health"));
        HUD_Assets.Add(HUD_Objects.PLAYER_HEALTH_SUBBG, Resources.Load<Sprite>("Images/HUD/Enemy/Health_SubBg"));
        HUD_Assets.Add(HUD_Objects.PLAYER_HEALTH_BG, Resources.Load<Sprite>("Images/HUD/Enemy/Health_Bg"));
        HUD_Assets.Add(HUD_Objects.PLAYER_LEFTPOSTURE, Resources.Load<Sprite>("Images/HUD/Enemy/Posture"));
        HUD_Assets.Add(HUD_Objects.PLAYER_RIGHTPOSTURE, Resources.Load<Sprite>("Images/HUD/Enemy/Posture"));
        HUD_Assets.Add(HUD_Objects.PLAYER_POSTURE_BG, Resources.Load<Sprite>("Images/HUD/Enemy/Posture_Bg"));
        HUD_Assets.Add(HUD_Objects.PLAYER_POSTURE_CENTER, Resources.Load<Sprite>("Images/HUD/Enemy/Posture_Center"));
        HUD_Assets.Add(HUD_Objects.BOSS_RIGHTPOSTURE, Resources.Load<Sprite>("Images/HUD/Enemy/Posture"));
        HUD_Assets.Add(HUD_Objects.BOSS_NAME_BG, Resources.Load<Sprite>("Images/HUD/Enemy/BossName_Bg"));

    }*/

    /// <summary>
    /// 移除清單
    /// </summary>
    /// <param name="enemy"></param>
    public void RemoveObject(GameObject obj)
    {
        Characters.Remove(obj);
        MainCameraControl.UnlockEnemy();
    }

    public void ShowDeathWindow()
    {
        if (UISources[Window.DEATH_WINDOW].activeSelf == true)
            return;
        UISources[Window.DEATH_WINDOW].SetActive(true);
    }

}
