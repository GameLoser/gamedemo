﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillInfo
{
    public SkillInfo(string skillName,Sprite skillIcon, Skills skillType, float magnification, int postureDamage, string deciption)
    {
        SkillName = skillName;
        SkillIcon = skillIcon;
        SkillType = skillType;
        Magnification = magnification;
        PostureDamage = postureDamage;
        Deciption = deciption;
    }

    public string SkillName = "";
    public Sprite SkillIcon = null;

    public Skills SkillType = Skills.NONE;
    public float Magnification = 0;
    public int PostureDamage = 0;

    public string Deciption = "";

}
