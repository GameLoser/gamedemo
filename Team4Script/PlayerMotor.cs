﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMotor : MonoBehaviour
{
    private CharacterController m_Controller = null;
    private CameraControl m_CameraControl = null;
    private PlayerInput m_PlayerInput = null;
    private MeleeWeaponTrail m_MeleeWeaponTrail = null;
    private Animator m_Animator = null;
    private AIStatus m_FatalStatus = null;

    public Vector3 MoveVector { get; set; }

    public float VerticalVelocity { get; set; }
    public float MoveValue { get; set; }

    //移動速度
    [SerializeField]
    private float m_RunSpeed = 6.0f;
    //跑速度
    [SerializeField]
    private float m_SpintSpeed = 15.0f;
    //重力
    [SerializeField]
    private float m_Gravity = 20.0f;
    [SerializeField]
    private float m_TerminalVelocity = 10f;
    [SerializeField]
    public Collider WeaponCollider { get; private set; }
    [SerializeField]
    private Transform m_HoldBlade = null;
    //
    //[SerializeField]
    //private float m_SlopeForceRayLength = 2f;
    ////
    //[SerializeField]
    //private float m_SlopeForce = 40f;

    private bool m_IsSprint = false;
    private bool m_IsDodge = false;
    private bool m_IsFatalAttack = false;
    private bool m_IsStopRotate = false;

    private Quaternion m_CurrentRotation;

    private GameObject m_FatalTarget = null;
    private SkillInfo m_CurrentEquitSkill = null;
    private SkillInfo m_NormalAtk = null;

    void Start()
    {
        //m_CameraControl = MainSystem.Instance.CAssetsPool.CameraSet.GetComponentInChildren<CameraControl>();
        m_CameraControl = MainSystem.Instance.CAssetsPool.MainCameraControl;
        m_PlayerInput = GetComponent<PlayerInput>();
        m_Controller = GetComponent<CharacterController>();
        m_Animator = GetComponent<Animator>();
        m_NormalAtk = new SkillInfo("", null, Skills.NONE, 1, 25,"");
        if (m_HoldBlade != null)
        {
            WeaponCollider = m_HoldBlade.Find("hitter").GetComponent<Collider>();
            m_MeleeWeaponTrail = m_HoldBlade.Find("Trailer").GetComponent<MeleeWeaponTrail>();
        }

        m_CurrentRotation = transform.rotation;
    }

    public void Update()
    {
        m_PlayerInput.PlayerInputUpdate();

        UpdateAnimator();
        ProcessMotion();
    }

    void UpdateAnimator()
    {
        MoveValue = Mathf.Lerp(0, 1, MoveValue);
        PlayerAnimator.AnimatorMove(m_Animator, MoveValue);
        if (m_CameraControl.IsLockOn == true)
            PlayerAnimator.AnimatorLockMove(m_Animator, MoveVector.x, MoveVector.z);

        //WeaponCollider.SendMessage("EnableFatalAttack", 
        //    PlayerAnimator.CurrentAnimationState(m_Animator) == PlayerState.FATAL_ATTACK ? true : false);

        PlayerAnimator.AnimatorIsLock(m_Animator, m_CameraControl.IsLockOn);
    }
    /// <summary>
    /// 動畫事件攻擊時開啟碰撞
    /// </summary>
    /// <param name="isAttack"></param>
    public void AnimationEventOnAttack(int isAttack)
    {
        if (WeaponCollider != null)
        {
            WeaponCollider.enabled = isAttack > 0 ? true : false;
            m_MeleeWeaponTrail.Emit = WeaponCollider.enabled;
            PlayerAnimator.AnimatorSkill(m_Animator, 0);
            if(PlayerAnimator.AnimatorCancelFetal(m_Animator, m_FatalStatus) == false)
                m_IsFatalAttack = false;

        }
    }
    /// <summary>
    /// 動畫事件播放某一個動作會旋轉武器
    /// </summary>
    /// <param name="isRotate"></param>
    void AnimationEventRotateBlade(int isRotate)
    {
        if (m_HoldBlade != null)
        {
            if (isRotate > 0)
                m_HoldBlade.localRotation = Quaternion.Euler(new Vector3(0f, 180f, 215f));
            else
                m_HoldBlade.rotation = new Quaternion(0, 0, 0, 0);
        }
    }
    /// <summary>
    /// 動畫事件取消處刑時無法攻擊的狀態
    /// </summary>
    void AnimationEventUnlockAttack() { m_IsFatalAttack = false; }

    void AnimationEventOnDead()
    {
        MainSystem.Instance.CBattleSystem.RemoveList(this.gameObject);
        AnimationEventOnAttack(0);
        MainSystem.Instance.EGameState = GameState.GAMEOVER;
    }

    void AnimationEventFalalKill()
    {
        MainSystem.Instance.CBattleSystem.SomeoneFatalDead(this.gameObject, m_FatalTarget);
    }

    void ProcessMotion()
    {
        SnapAlignCharacterWithCamera();
        //將自身本地座標轉至世界座標
        MoveVector = transform.TransformDirection(MoveVector);
        //將移動單位化
        if (MoveVector.magnitude > 1)
            MoveVector = Vector3.Normalize(MoveVector);
        //
        FaceToMoveSide();
        //慣性運動
        InertialMotion(PlayerAnimator.CurrentAnimationState(m_Animator));
        //加速度
        MoveVector *= MoveSpeed();
        //角色Y軸向量控制
        MoveVector = new Vector3(MoveVector.x, VerticalVelocity, MoveVector.z);
        //重力
        ApplyGravity();
        //斜坡平滑落下
        //OnSlope();
        //
        m_Controller.Move(MoveVector * Time.deltaTime);
    }
    /// <summary>
    /// 對應攝影機視角轉向
    /// </summary>
    void SnapAlignCharacterWithCamera()
    {
        if (MoveVector.x != 0 || MoveVector.z != 0 || m_CameraControl.IsLockOn == true)
            transform.rotation = Quaternion.Euler(transform.eulerAngles.x, MainSystem.Instance.CAssetsPool.MainCameraControl.transform.eulerAngles.y, transform.eulerAngles.z);
    }
    /// <summary>
    /// 面相移動方向
    /// </summary>
    void FaceToMoveSide()
    {
        if (m_CameraControl.IsLockOn == false && m_IsStopRotate == false)
        {
            if (MoveVector.x != 0 || MoveVector.z != 0)
            {
                m_CurrentRotation = Quaternion.Slerp(m_CurrentRotation, Quaternion.LookRotation(MoveVector, Vector3.up), MoveValue * 30 * Time.deltaTime);
                transform.rotation = m_CurrentRotation;
            }
        }
    }
    /// <summary>
    /// 慣性運動
    /// </summary>
    /// <param name="ps"></param>
    void InertialMotion(PlayerState ps)
    {
        Vector3 dir = Vector3.zero;
        switch (ps)
        {
            case PlayerState.DODGE:
                MoveVector = transform.forward;
                break;
            case PlayerState.ATTACK:
            case PlayerState.SKILLATTACK:
                var target = m_CameraControl.TargetLookAt;
                if (target.tag == "Enemy")
                {
                    dir = new Vector3(target.position.x - transform.position.x, 0, target.position.z - transform.position.z);
                    InstantRotate(dir);
                }
                break;
            case PlayerState.FATAL_ATTACK:
                m_IsStopRotate = true;
                if(m_FatalTarget != null)
                    dir = new Vector3(m_FatalTarget.transform.position.x - transform.position.x, 0, m_FatalTarget.transform.position.z - transform.position.z);
                InstantRotate(dir);
                break;
            case PlayerState.REST:
                m_IsStopRotate = true;
                break;
            default:
                m_IsStopRotate = false;
                break;
        }
    }

    void InstantRotate(Vector3 dir)
    {
        m_CurrentRotation = Quaternion.LookRotation(dir, Vector3.up);
        transform.rotation = m_CurrentRotation;
    }
    /// <summary>
    /// 移動速度
    /// </summary>
    /// <returns></returns>
    float MoveSpeed()
    {
        var speed = m_IsSprint ? m_SpintSpeed : m_RunSpeed;
        switch (PlayerAnimator.CurrentAnimationState(m_Animator))
        {
            case PlayerState.DODGE:
                speed = 8;
                break;
            case PlayerState.SPRINT_TO_STOP:
            case PlayerState.ATTACK:
            case PlayerState.SKILLATTACK:
            case PlayerState.FATAL_ATTACK:
            case PlayerState.LOCK_DODGE:
            case PlayerState.DEAD:
            case PlayerState.GETHIT:
            case PlayerState.GUARDGETHIT:
            case PlayerState.OUT_OF_STAMINA:
            case PlayerState.USE:
            case PlayerState.REST:
                speed = 0f;
                break;
            case PlayerState.DEFENCE:
                speed = 0f;
                if (PlayerAnimator.CurrentAnimationStateLayer2(m_Animator) == PlayerState.WALK)
                    speed = 0.7f;
                break;
        }

        return speed;
    }
    /// <summary>
    /// 重力
    /// </summary>
    void ApplyGravity()
    {
        if (MoveVector.y > -m_TerminalVelocity)
            MoveVector = new Vector3(MoveVector.x, MoveVector.y - m_Gravity * Time.deltaTime, MoveVector.z);

        if (m_Controller.isGrounded && MoveVector.y < -1)
            MoveVector = new Vector3(MoveVector.x, -1, MoveVector.z);
    }
    //private void OnSlope()
    //{
    //    if (m_IsFalling == true) return;

    //    RaycastHit hitInfo;

    //    if (Physics.Raycast(transform.position, Vector3.down, out hitInfo, (m_Controller.height * 0.5f) * m_SlopeForceRayLength))
    //    {
    //        if (hitInfo.normal != Vector3.up)
    //            m_MoveVector += Vector3.down * (m_Controller.height * 0.5f) * m_SlopeForce;
    //    }
    //}
    /// <summary>
    /// 攻擊
    /// </summary>
    public void Attack()
    {
        if (m_IsFatalAttack == true) return;
        RaycastHit hitInfo;
        
        var dir = transform.TransformDirection(Vector3.forward);
        var fatalDir = 0;

        if (Physics.Raycast(transform.position + Vector3.up, dir, out hitInfo, 2.5f))
        {
            if (hitInfo.collider.tag == "Enemy")
            {
                m_FatalStatus = hitInfo.collider.GetComponent<AIStatus>();
                var enemyDir = Vector3.Dot(transform.position - m_FatalStatus.transform.position, m_FatalStatus.transform.forward);
                m_IsFatalAttack = m_FatalStatus.FatalKill;

                if (enemyDir < 0) fatalDir = 0;
                else fatalDir = 2;

                if (m_IsFatalAttack == true) m_FatalTarget = hitInfo.collider.gameObject;
                else m_FatalTarget = null;
            }
        }

        if(m_IsFatalAttack == true)
        {
            PlayerAnimator.AnimatorFetalAttack(m_Animator, fatalDir);
            m_FatalStatus.FatalingKill(this.transform);
        }
        else
        {
            if (m_Animator.GetBool("IsDefense") == true && m_CurrentEquitSkill != null)
            {
                PlayerAnimator.AnimatorSkill(m_Animator, (int)m_CurrentEquitSkill.SkillType);
                this.gameObject.SendMessage("CalculateDamage", m_CurrentEquitSkill);
            }
            else
            {
                PlayerAnimator.AnimatorAttack(m_Animator);
                this.gameObject.SendMessage("CalculateDamage", m_NormalAtk);
            }
                
        }

    }
    /// <summary>
    /// 防禦
    /// </summary>
    /// <param name="isDefense"></param>
    public void Defense(bool isDefense) { PlayerAnimator.AnimatorDefense(m_Animator, isDefense); }
    /// <summary>
    /// 防禦時可移動
    /// </summary>
    /// <param name="i"></param>
    void AnimationEventCanMoveWhenDefense(int i) { PlayerAnimator.AnimatorDefenseMove(m_Animator, i > 1 ? true : false); }
    /// <summary>
    /// 閃避
    /// </summary>
    /// <param name="isSprint">持續衝刺</param>
    /// <param name="isDodge">閃避</param>
    public void Dodge(bool isSprint, bool isDodge)
    {
        m_IsSprint = isSprint;
        m_IsDodge = isDodge;
        PlayerAnimator.AnimatorSprintDodge(m_Animator, m_IsSprint, m_IsDodge);
    }

    public void RecieveEquitSkill(SkillInfo info)
    {
        m_CurrentEquitSkill = info;
    }

}
