﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageInfo
{
    public GameObject Hitter = null;
    public int HealthDamage = 0;
    public int PostureDamage = 0;

}
