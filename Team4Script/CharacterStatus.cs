﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStatus : MonoBehaviour
{
    public int HealthPoint { get; protected set; }
    public int MaxHealthPoint { get; protected set; }
    public int Posture { get; protected set; }
    public int MaxPosture { get; protected set; }
    public float AlertRatio { get; protected set; }
    public virtual bool FatalKill { get; protected set; }
    public AI_AlertLevel AlertLevel { get; protected set;}
    public AI_Rank Rank { get; protected set; }
    public AI_VitalStatus VitalStatus { get; protected set; }
    public int NumbersOfLife { get; protected set; }

    public DamageInfo DamageInfo;
    /// <summary>
    /// 受到血量傷害
    /// </summary>
    /// <param name="damage"></param>
    public virtual void GetDamage(DamageInfo info, Vector3 hitPos) { }
    /// <summary>
    /// 受到軀幹值傷害
    /// </summary>
    /// <param name="damage"></param>
    public virtual void GetIncreasePosture(DamageInfo info) { }

    public virtual void PlayFatalDeadAnimator(GameObject hitter) { }

}
