﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class UIWelcomeView : MonoBehaviour
{
    private MainSystem m_MainSystem = null;
    private bool m_IsOneCall = false;

    [SerializeField]
    private GameObject OptionWindow = null;
    [SerializeField]
    private GameObject LoadingPanel = null;
    [SerializeField]
    private GameObject BlackWindow = null;
    [SerializeField]
    private GameObject TileWindow = null;
    [SerializeField]
    private GameObject BlackMask = null;
    [SerializeField]
    private Image LoadingImage = null;

    [SerializeField]
    private Toggle YInverse = null;
    [SerializeField]
    private Image MouseSensity = null;
    [SerializeField]
    private Image BGMVolume = null;
    [SerializeField]
    private Image SoundVolume = null;


    [SerializeField]
    private VideoPlayer OpenVideo = null;

    private void Start()
    {
        m_MainSystem = MainSystem.Instance;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        YInverse.isOn = m_MainSystem.CRecordData.IsInverseYMove;
        MouseSensity.fillAmount = m_MainSystem.CRecordData.MouseSensity * 0.1f;
        BGMVolume.fillAmount = m_MainSystem.CRecordData.BGMVolume;
        SoundVolume.fillAmount = m_MainSystem.CRecordData.SoundVolume;
    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            OptionWindow.SetActive(false);
        }
    }

    public void ButtonClick(Button button)
    {
        switch(button.gameObject.name)
        {
            case "New Game":
                if (m_IsOneCall == false)
                {
                    BlackWindow.SetActive(true);
                    //LoadingPanel.SetActive(true);
                    //StartCoroutine(m_MainSystem.CSceneSystem.LoadScene("MainGameScene", LoadingImage, m_MainSystem.InitSystem));
                    m_IsOneCall = true;
                }
                break;
            case "Continue":
                if (m_IsOneCall == false && m_MainSystem.CRecordData.SavePostion != Vector3.zero)
                {
                    LoadingPanel.SetActive(true);
                    StartCoroutine(m_MainSystem.CSceneSystem.LoadScene("MainGameScene", LoadingImage, m_MainSystem.LoadSystem));
                    m_IsOneCall = true;
                }
                break;
            case "Options":
                OptionWindow.SetActive(true);
                break;
            case "Quit Game":
                Application.Quit();
                break;
            case "MouseSensityLeft":
                if (MouseSensity.fillAmount > 0)
                {
                    MouseSensity.fillAmount -= 0.1f;
                    m_MainSystem.CRecordData.MouseSensity -= 1;
                }
                break;
            case "MouseSensityRight":
                if (MouseSensity.fillAmount <= 1)
                {
                    MouseSensity.fillAmount += 0.1f;
                    m_MainSystem.CRecordData.MouseSensity += 1;
                }
                break;
            case "BGMLeft":
                if (BGMVolume.fillAmount > 0)
                {
                    BGMVolume.fillAmount -= 0.1f;
                    m_MainSystem.CRecordData.BGMVolume -= 0.1f;
                }
                break;
            case "BGMRight":
                if (BGMVolume.fillAmount <= 1)
                {
                    BGMVolume.fillAmount += 0.1f;
                    m_MainSystem.CRecordData.BGMVolume += 0.1f;
                }
                break;
            case "SoundLeft":
                if (SoundVolume.fillAmount > 0)
                {
                    SoundVolume.fillAmount -= 0.1f;
                    m_MainSystem.CRecordData.SoundVolume -= 0.1f;
                }
                break;
            case "SoundRight":
                if (SoundVolume.fillAmount <= 1)
                {
                    SoundVolume.fillAmount += 0.1f;
                    m_MainSystem.CRecordData.SoundVolume += 0.1f;
                }
                break;
        }
    }

    public void ToggleChange(Toggle toggle)
    {
        m_MainSystem.CRecordData.IsInverseYMove = toggle.isOn;
    }

    void EndReached(VideoPlayer vp)
    {
        LoadingPanel.SetActive(true);
        StartCoroutine(m_MainSystem.CSceneSystem.LoadScene("MainGameScene", LoadingImage, m_MainSystem.InitSystem));
    }

    public void ResetTweenAlpha(uTools.uTweenAlpha alpha)
    {
        alpha.Reset02();
        alpha.RemoveOnMiddle();
    }

    public void MiddleEvent()
    {
        TileWindow.SetActive(false);
        BlackMask.SetActive(true);
        OpenVideo.transform.root.gameObject.SetActive(true);
        OpenVideo.loopPointReached += EndReached;
    }

}
