﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    private PlayerMotor m_PlayerMotor = null;
    private CameraControl m_CameraControl = null;
    private ItemInfo CurrentItem = null;
    private PlayerStatus m_PlayerStatus = null;

    void Start ()
    {
        m_PlayerMotor = GetComponent<PlayerMotor>();
        m_PlayerStatus = GetComponent<PlayerStatus>();
        m_CameraControl = MainSystem.Instance.CAssetsPool.MainCameraControl;
    }
    /// <summary>
    /// 玩家角色輸入控制
    /// </summary>
    public void PlayerInputUpdate()
    {
        if (MainSystem.Instance.CGameStateInput.IsStopPlayerInput == false)
        {
            PlayerMoveInput();
            HandleActionInput();
        }
        else
            ResetInput();
    }

    void SettingCurrentItem(ItemInfo info) { CurrentItem = info; }

    public void LockOnEnemy() { m_CameraControl.LockOnEnemy(); }

    private void PlayerMoveInput()
    {
        var deadZone = 0.1f;

        m_PlayerMotor.VerticalVelocity = m_PlayerMotor.MoveVector.y;
        m_PlayerMotor.MoveVector = Vector3.zero;

        m_PlayerMotor.MoveValue = Mathf.Abs(Input.GetAxis("Vertical")) + Mathf.Abs(Input.GetAxis("Horizontal"));

        if (Input.GetAxis("Vertical") > deadZone || Input.GetAxis("Vertical") < -deadZone)
            m_PlayerMotor.MoveVector += new Vector3(0, 0, Input.GetAxis("Vertical"));
        if (Input.GetAxis("Horizontal") > deadZone || Input.GetAxis("Horizontal") < -deadZone)
            m_PlayerMotor.MoveVector += new Vector3(Input.GetAxis("Horizontal"), 0, 0);
    }

    void HandleActionInput()
    {
        if (Input.GetMouseButtonDown(0)) Attack();

        Defense(Input.GetMouseButton(1));

        if (Input.GetMouseButtonDown(2)) LockOnEnemy();

        if (Input.GetKeyDown(KeyCode.F)) m_PlayerStatus.GetSomeStatusChange(CurrentItem);

        Dodge();
    }

    void Attack() { m_PlayerMotor.Attack(); }

    void Defense(bool isDefense) { m_PlayerMotor.Defense(isDefense); }

    void Dodge()
    {
        var sprint = Input.GetKey(KeyCode.LeftShift) ? true : false;
        var dodge = Input.GetKeyDown(KeyCode.LeftShift) ? true : false;
        m_PlayerMotor.Dodge(sprint, dodge);
    }

    private void ResetInput()
    {
        m_PlayerMotor.MoveVector = Vector3.zero;
        m_PlayerMotor.MoveValue = 0;
    }

}
