﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePointSet : MonoBehaviour
{
    private Animator m_Animator = null;

    private void Start()
    {
        foreach(var o in MainSystem.Instance.CAssetsPool.Characters)
        {
            if (o.tag == "Player") m_Animator = o.GetComponent<Animator>();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            if(Vector3.Dot(other.transform.forward, transform.position - other.transform.position) > 0)
            {
                MainSystem.Instance.CAssetsPool.ObjectsPointer.GamePlayUse.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    PlayerAnimator.AnimatorRest(m_Animator);
                    MainSystem.Instance.CRecordData.SavePostion = this.transform.position + Vector3.back;
                    MainSystem.Instance.CRecordData.SaveRotation = this.transform.rotation;
                    MainSystem.Instance.CAudioSystem.PlayAudio(AudioName.MAP_MESSAGE, 1.0f, 1.6f);
                    MainSystem.Instance.CEffectSystem.PlayEffect(EffectName.SAVE_WAVE, transform.position, 1.6f);
                    MainSystem.Instance.CAssetsPool.ObjectsPointer.ResetItems();
                    foreach (var o in MainSystem.Instance.CAssetsPool.Characters)
                    {
                        if (o.tag == "Player") o.SendMessage("ResetStatus");
                    }
                }
            }
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        MainSystem.Instance.CAssetsPool.ObjectsPointer.GamePlayUse.SetActive(false);
    }

}
