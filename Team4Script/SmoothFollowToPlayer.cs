﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollowToPlayer : MonoBehaviour
{
    private Transform m_FollowTarget = null;
    [SerializeField]
    private float m_Height = 0;

    private Vector3 m_FollowHeight = Vector3.zero;
    private Vector3 m_VelFollow = Vector3.zero;

    private void Start()
    {
        foreach(var o in MainSystem.Instance.CAssetsPool.Characters)
        {
            if(o.tag == "Player") m_FollowTarget = o.transform;
        }
        if (m_FollowTarget == null)
            return;
        //transform.position = m_FollowTarget.position;
        m_Height += m_FollowTarget.GetComponent<CharacterController>().height * 0.5f;
        m_FollowHeight = new Vector3(0, m_Height, 0);
    }

    void Update ()
    {
        if (m_FollowTarget == null)
            return;
        transform.position = Vector3.SmoothDamp(transform.position, m_FollowTarget.position + m_FollowHeight, ref m_VelFollow, 0.15f);
    }
}
