﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMainMenu : MonoBehaviour
{
    private Image m_LastSelectTab = null;

    private UIObjectsPointer m_ObjectsPointer = null;
    private MainSystem m_MainSystem = MainSystem.Instance;

    void Start ()
    {
        m_LastSelectTab = m_MainSystem.CAssetsPool.UISources[Window.MAIN_MENU_WINDOW].transform.Find("MainMenu-GridPage/EquitWindow").GetComponent<Image>();
        m_ObjectsPointer = m_MainSystem.CAssetsPool.UISources[Window.MAIN_WINDOW].GetComponent<UIObjectsPointer>();
    }


    public void TabPageSelect(Image image)
    {
        if (m_LastSelectTab != null)
        {
            m_LastSelectTab.sprite = m_MainSystem.CAssetsPool.ImageAssets[AssetsSprite.TAB_NORMAL];
            switch(m_LastSelectTab.gameObject.name)
            {
                case "EquitWindow":
                    m_MainSystem.CAssetsPool.UISources[Window.EQUIT_WINDOW].SetActive(false);
                    break;
                case "SettingsWindow":
                    m_MainSystem.CAssetsPool.UISources[Window.SETTINGS_WINDOW].SetActive(false);
                    break;
            }
        }
        m_LastSelectTab = image;
        image.sprite = m_MainSystem.CAssetsPool.ImageAssets[AssetsSprite.TAB_HIGHLIGHT];
        switch (m_LastSelectTab.gameObject.name)
        {
            case "EquitWindow":
                m_MainSystem.CAssetsPool.UISources[Window.EQUIT_WINDOW].SetActive(true);
                break;
            case "SettingsWindow":
                m_MainSystem.CAssetsPool.UISources[Window.SETTINGS_WINDOW].SetActive(true);
                break;
        }
    }

    public void ButtonClick(Button button)
    {
        if (button.name.Contains("Item"))
        {
            m_ObjectsPointer.LastClickButtonImage = button.transform.Find("Image").GetComponent<Image>();
            m_MainSystem.CAssetsPool.UISources[Window.ITEM_SELECT_WINDOW].SetActive(true);
            m_MainSystem.CGameStateInput.CurrentActiveWindow = m_MainSystem.CAssetsPool.UISources[Window.ITEM_SELECT_WINDOW];
        }
        else if (button.name.Contains("Skill"))
        {
            m_ObjectsPointer.LastClickButtonImage = button.transform.Find("Image").GetComponent<Image>();
            m_MainSystem.CAssetsPool.UISources[Window.SKILL_SELECT_WINDOW].SetActive(true);
            m_MainSystem.CGameStateInput.CurrentActiveWindow = m_MainSystem.CAssetsPool.UISources[Window.SKILL_SELECT_WINDOW];
        }
        else if (button.name.Contains("ReturnTitle"))
        {
            m_MainSystem.CAssetsPool.UISources[Window.CONFIRM_WINDOW].SetActive(true);
            m_MainSystem.CGameStateInput.CurrentActiveWindow = m_MainSystem.CAssetsPool.UISources[Window.CONFIRM_WINDOW];
        }
        else if (button.name.Contains("Yes"))
        {
            m_MainSystem.CAssetsPool.UISources[Window.LOAD_WINDOW].SetActive(true);
            m_MainSystem.CRecordData.SaveData(m_MainSystem.CAssetsPool.ObjectsPointer.ItemDictionary, m_MainSystem.CGameStateInput.ItemList);
            StartCoroutine(m_MainSystem.CSceneSystem.LoadScene("WelcomeScene", m_ObjectsPointer.LoadBarImage, m_MainSystem.WelcomeSceneSet));
            m_MainSystem.CAssetsPool.UISources[Window.CONFIRM_WINDOW].SetActive(false);
        }
        else if (button.name.Contains("No"))
        {
            m_MainSystem.CAssetsPool.UISources[Window.CONFIRM_WINDOW].SetActive(false);
            m_MainSystem.CGameStateInput.CurrentActiveWindow = m_MainSystem.CAssetsPool.UISources[Window.MAIN_MENU_WINDOW];
        }
        else if (button.name.Contains("CameraAndSounds"))
        {
            m_MainSystem.CAssetsPool.UISources[Window.OPTION_VOLUME_WINDOW].SetActive(true);
            m_MainSystem.CGameStateInput.CurrentActiveWindow = m_MainSystem.CAssetsPool.UISources[Window.OPTION_VOLUME_WINDOW];
        }
        else if (button.name.Contains("MouseSensityLeft"))
        {
            if(m_ObjectsPointer.MouseSensity.fillAmount > 0)
            {
                m_ObjectsPointer.MouseSensity.fillAmount -= 0.1f;
                m_MainSystem.CRecordData.MouseSensity -= 1;
                m_MainSystem.CAssetsPool.MainCameraControl.MouseSensitivityX -= 1;
                m_MainSystem.CAssetsPool.MainCameraControl.MouseSensitivityY -= 1;
            }
        }
        else if (button.name.Contains("MouseSensityRight"))
        {
            if (m_ObjectsPointer.MouseSensity.fillAmount <= 1)
            {
                m_ObjectsPointer.MouseSensity.fillAmount += 0.1f;
                m_MainSystem.CRecordData.MouseSensity += 1;
                m_MainSystem.CAssetsPool.MainCameraControl.MouseSensitivityX += 1;
                m_MainSystem.CAssetsPool.MainCameraControl.MouseSensitivityY += 1;
            } 
        }
        else if (button.name.Contains("BGMLeft"))
        {
            if (m_ObjectsPointer.BGMVolume.fillAmount > 0)
            {
                m_ObjectsPointer.BGMVolume.fillAmount -= 0.1f;
                m_MainSystem.CRecordData.BGMVolume -= 0.1f;
                MainSystem.Instance.CBgmSystem.VolumeUpdate();
            } 
        }
        else if (button.name.Contains("BGMRight"))
        {
            if (m_ObjectsPointer.BGMVolume.fillAmount <= 1)
            {
                m_ObjectsPointer.BGMVolume.fillAmount += 0.1f;
                m_MainSystem.CRecordData.BGMVolume += 0.1f;
                MainSystem.Instance.CBgmSystem.VolumeUpdate();
            }
        }
        else if (button.name.Contains("SoundLeft"))
        {
            if (m_ObjectsPointer.SoundVolume.fillAmount > 0)
            {
                m_ObjectsPointer.SoundVolume.fillAmount -= 0.1f;
                m_MainSystem.CRecordData.SoundVolume -= 0.1f;
            }  
        }
        else if (button.name.Contains("SoundRight"))
        {
            if (m_ObjectsPointer.SoundVolume.fillAmount <= 1)
            {
                m_ObjectsPointer.SoundVolume.fillAmount += 0.1f;
                m_MainSystem.CRecordData.SoundVolume += 0.1f;
            }
        }

    }

    public void ToggleChange(Toggle toggle)
    {
        m_MainSystem.CRecordData.IsInverseYMove = toggle.isOn;
        m_MainSystem.CAssetsPool.MainCameraControl.YInverse = toggle.isOn;
    }

    

}
