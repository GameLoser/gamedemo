﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInfo 
{
    public ItemInfo(ItemType type, string code, Sprite itemIcon, string name,
        int maxAmout, int healPoint, int addAtk, int addDefence, float itemTime, 
        string depiction, EffectName effectType, AudioName audio)
    {
        TheItemType = type;
        ItemCode = code;
        ItemIcon = itemIcon;
        ItemName = name;
        MaxAmount = maxAmout;
        HealPoint = healPoint;
        AddTempAttack = addAtk;
        AddTempPostureDefence = addDefence;
        ItemTime = itemTime;

        Depiction = depiction;
        EffectType = effectType;
        AudioType = audio;
    }

    public ItemType TheItemType = ItemType.NONE;
    public string ItemCode = "";
    public Sprite ItemIcon = null;
    public string ItemName = "";
    public int CurrentAmount = 0;
    public int MaxAmount = 0;

    public string Depiction = "";

    public int HealPoint = 0;
    public int AddTempAttack = 0;
    public int AddTempPostureDefence = 0;

    public EffectName EffectType = EffectName.NONE;
    public AudioName AudioType = AudioName.NONE;

    public float ItemTime = 0;
}
