﻿
public enum GameState
{
    NONE,
    WELCOMEVIEW,
    GAMEPLAY,
    GAMEOVER,
    GAMECOMPLETE,
    NEWGAMEMOVIE
}

public enum PlayerState
{
    NONE = -1,
    IDLE,
    WALK,
    RUN,
    SPRINT,
    DODGE,
    SPRINT_TO_STOP,
    ATTACK,
    SKILLATTACK,
    DEFENCE,
    LOCK_DODGE,
    FATAL_ATTACK,
    DEAD,
    GETHIT,
    GUARDGETHIT,
    OUT_OF_STAMINA,
    USE,
    LOCK_ON_RUN,
    REST
}

public enum Skills
{
    NONE,
    SKILL01,
    SKILL02,
    SKILL03
}

public enum ItemType
{
    NONE,
    HEAL,
    ENCHANT,
    ATTACK_UP_TEMP,
    POSTUREDEFENCE_UP_TEMP
}

public enum MouseMoveSide
{
    NONE,
    UP,
    DOWN,
    RIGHT,
    LEFT
}


public enum AI_AlertLevel
{
    NONE,
    WHITE,
    YELLOW,
    RED
}

public enum Window
{
    NONE,
    MAIN_WINDOW,
    DEATH_WINDOW,
    MAIN_MENU_WINDOW,
    EQUIT_WINDOW,
    SETTINGS_WINDOW,
    OPTION_VOLUME_WINDOW,
    ITEM_SELECT_WINDOW,
    SKILL_SELECT_WINDOW,
    CONFIRM_WINDOW,
    GAMEPLAY_WINDOW,
    LOAD_WINDOW,
    AREA_WINDOW,
    BLACK_FADE,
    BOSS_KILLED
}

public enum AssetsSprite
{
    NONE,
    NULL,
    TAB_NORMAL,
    TAB_HIGHLIGHT,
    SKILL01,
    SKILL02,
    SKILL03,
    HEAL_BOTTLE,
    HEAL_DRUG,
    ATTACKSUGER,
    POSTURESUGER,
    ITEM_BG,
    SKILL_BG
}

public enum Character
{
    Player,
}

public enum AudioName
{
    NONE,
    BLOCK1,
    BLOCK2,
    BLOCK3,
    BLOCK4,
    DEFLECTION1,
    DEFLECTION2,
    DEFLECTION3,
    DEFLECTION4,
    DEFLECTION5,
    DEFLECTION6,
    DEFLECTION7,
    DEFLECTION8,
    DEFLECTION9,
    DEFLECTION10,
    GET_HIT1,
    GET_HIT2,
    ALERT,
    ALERTED,
    HEAL,
    BUFF,
    MAP_MESSAGE,
    CAN_FATAL_KILL,
    CAN_FATAL_KILL2,
    FATAL_KILL,
    EXECUTION,
    SNEAK_SCENE,
    ALERT_SCENE,
    FIGHT_SCENE,
    MIDDLE_BOSS_SCENE,
    STAGE_BOSS_SCENE,
}

public enum EffectName
{
    NONE,
    SPARKS1,
    SPARKS2,
    CIRCLE_SPARKS1,
    CIRCLE_SPARKS2,
    HEAL_WAVE1,
    ATTACK_BUFF,
    POSTURE_BUFF,
    SAVE_WAVE,

}

public enum AI_Rank
{
    MINIONS,
    MIDDLE_BOSS,
    STAGE_BOSS
}

public enum AI_VitalStatus
{
    ALIVE,
    DEAD,    
}

public enum HUD_Objects
{
    // Enemy
    // Health Bar
    HEALTH,
    HEALTH_SUBBG,
    HEALTH_BG,
     
    // Posture Bar
    POSTURE_LEFT,
    POSTURE_RIGHT,
    POSTURE_BG,
    POSTURE_CENTER,

    // Detect
    DETECT_WHITE,
    DETECT_WHITE_BG,
    DETECT_YELLOW,
    DETECT_RED,
    
        

    // DetectIndicator
    DETECTINDI_WHITE,
    DETECTINDI_YELLOW,
    DETECTINDI_RED,

    // Focus
    FOCUS,

    // Fatal Kill
    FATALKILL,

    // Boss
    // Boss Health Bar
    BOSS_HEALTH,
    BOSS_HEALTH_SUBBG,
    BOSS_HEALTH_BG,

    // Boss Posture Bar
    BOSS_LEFTPOSTURE,
    BOSS_RIGHTPOSTURE,
    BOSS_POSTURE_BG,
    BOSS_POSTURE_CENTER,

    // Name
    BOSS_NAME_BG,

    // MainPlayer
    // Health
    PLAYER_HEALTH,
    PLAYER_HEALTH_SUBBG,
    PLAYER_HEALTH_BG,

    // Posture
    PLAYER_LEFTPOSTURE,
    PLAYER_RIGHTPOSTURE,
    PLAYER_POSTURE_BG,
    PLAYER_POSTURE_CENTER
}


public enum HUD_Name
{
    HUD_MAIN_PLAYER,
    HUD_ENEMY,
    HUD_CANVAS
}