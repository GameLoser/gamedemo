﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitter : MonoBehaviour
{
    private Collider m_Collider = null;
    private bool m_IsFatalAttack = false;
    private BattleSystem m_BattleSystem = null;

    private void Start()
    {
        m_Collider = GetComponent<Collider>();
        m_BattleSystem = MainSystem.Instance.CBattleSystem;
    }

    //void EnableFatalAttack(bool b) { m_IsFatalAttack = b; }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("OnTriggerEnter");
        //Vector3 hitPosition = Vector3.zero;
        if (other.tag == "Enemy")
        {
            if(m_IsFatalAttack == false)
            {
                Vector3 hitPosition = other.ClosestPoint(transform.position);
                m_BattleSystem.SomeonePostureIncrease(this.transform.root.gameObject, other.gameObject);
                m_BattleSystem.SomeoneGetDamage(this.transform.root.gameObject, other.gameObject, hitPosition);
            }
            else
            {
                m_BattleSystem.SomeoneFatalDead(this.transform.root.gameObject, other.gameObject);
                //m_BattleSystem.SomeoneGetDamage(this.transform.root.gameObject, other.gameObject, hitPosition);
            }
                
        }
    }

    private void OnTriggerExit(Collider other)
    {
        m_Collider.enabled = false;
    }

    //void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.collider.tag == "Enemy")
    //        BattleSystem.Instance.SomeoneGetDamage(this.transform.root.gameObject, collision.gameObject);
    //}

    //void OnCollisionExit(Collision collision)
    //{
    //    m_Collider.enabled = false;
    //}

}
