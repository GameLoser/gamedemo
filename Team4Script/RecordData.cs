﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecordData
{
    public Vector3 SavePostion = Vector3.zero;
    public Quaternion SaveRotation = new Quaternion();
    public Dictionary<Sprite, ItemInfo> SaveItem = new Dictionary<Sprite, ItemInfo>();
    public List<ItemInfo> SaveEquitItem = new List<ItemInfo>();
    public SkillInfo SaveEquitSkill = null;

    public string[] CurrentMission = new string[]
        {
            "與眼前的男人對話",
            "潛入玄夜城",
            "調查玄夜城",
            "擊敗主謀"
        };

    public bool IsInverseYMove = true;
    public float MouseSensity = 4;
    public float BGMVolume = 1;
    public float SoundVolume = 1;

    public void ResetSaveData()
    {
        SavePostion = new Vector3(290, 5f, 197);
        //SavePostion = new Vector3(264, 33f, 62f);
        //SavePostion = new Vector3(284f, 30f, 210f);
        //SavePostion = new Vector3(205f, 30f, 268f);
        //SavePostion = new Vector3(340f, 30f, 362f);


        SaveRotation = Quaternion.Euler(0, 168, 0);
        SaveItem.Clear();
        SaveEquitItem.Clear();
        SaveEquitSkill = null;
    }

    public void SaveData(Dictionary<Sprite, ItemInfo> saveItem, List<ItemInfo> saveEquitItem)
    {
        SaveItem.Clear();
        SaveEquitItem.Clear();
        SaveItem = saveItem;
        foreach(var o in saveEquitItem)
            SaveEquitItem.Add(o);
    }

}
