﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneSystem
{
    public SceneSystem()
    {
        
    }

    public IEnumerator LoadScene(string sceneName, Image loadImage, Action<AsyncOperation> callback = null)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
        asyncLoad.completed += callback;
        asyncLoad.allowSceneActivation = false;
        while (!asyncLoad.isDone)
        {
            loadImage.fillAmount = asyncLoad.progress * 100;
            if (asyncLoad.progress >= 0.9f)
                asyncLoad.allowSceneActivation = true;
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator LoadScene(string sceneName, Image loadImage, Action<AsyncOperation> callback = null, float delay = 0)
    {
        yield return new WaitForSeconds(delay);

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
        asyncLoad.completed += callback;

        asyncLoad.allowSceneActivation = false;
        while (!asyncLoad.isDone)
        {
            loadImage.fillAmount = asyncLoad.progress * 100;
            if (asyncLoad.progress >= 0.9f)
                asyncLoad.allowSceneActivation = true;
            yield return new WaitForEndOfFrame();
        }
    }

}
