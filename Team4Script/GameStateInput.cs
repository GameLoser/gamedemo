﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateInput
{
    public GameStateInput()
    {
        CurrentActiveWindow = null;
        IsStopPlayerInput = false;
        ItemList = new List<ItemInfo>();
    }

    private MainSystem m_MainSystem = MainSystem.Instance;

    //遊戲進行時目前所開啟的視窗
    public GameObject CurrentActiveWindow { get; set; }
    //是否停止玩家操作輸入
    public bool IsStopPlayerInput { get; set; }

    private UIObjectsPointer m_ObjectsPointer = null;
    private ItemInfo m_CurrentInfo = null;

    public List<ItemInfo> ItemList { get; private set; }

    public void Reset()
    {
        CurrentActiveWindow = null;
        IsStopPlayerInput = false;
        m_ObjectsPointer = MainSystem.Instance.CAssetsPool.ObjectsPointer;
        ItemList.Clear();
    }

    public void Menu()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Keypad7))
        {
            if (CurrentActiveWindow == null)
                CurrentActiveWindow = m_MainSystem.CAssetsPool.UISources[Window.MAIN_MENU_WINDOW];
            else if (CurrentActiveWindow != m_MainSystem.CAssetsPool.UISources[Window.MAIN_MENU_WINDOW])
            {
                CurrentActiveWindow.SetActive(false);
                CurrentActiveWindow = m_MainSystem.CAssetsPool.UISources[Window.MAIN_MENU_WINDOW];
                return;
            }

            m_MainSystem.CAssetsPool.UISources[Window.MAIN_MENU_WINDOW].SetActive(!m_MainSystem.CAssetsPool.UISources[Window.MAIN_MENU_WINDOW].activeSelf);

            if (m_MainSystem.CAssetsPool.UISources[Window.MAIN_MENU_WINDOW].activeSelf == true)
                Cursor.lockState = CursorLockMode.None;
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                CurrentActiveWindow = null;
            }

            Cursor.visible = m_MainSystem.CAssetsPool.UISources[Window.MAIN_MENU_WINDOW].activeSelf;
            IsStopPlayerInput = m_MainSystem.CAssetsPool.UISources[Window.MAIN_MENU_WINDOW].activeSelf;
        }
    }

    public void ChangeItem()
    {
        if (ItemList.Count <= 0) return;
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (ItemList.IndexOf(m_CurrentInfo) + 1 < ItemList.Count)
                m_CurrentInfo = ItemList[ItemList.IndexOf(m_CurrentInfo) + 1];
            else
                m_CurrentInfo = ItemList[0];

            m_ObjectsPointer.GamePlayItemImage.sprite = m_CurrentInfo.ItemIcon;
            m_ObjectsPointer.GamePlayItemText.text = m_ObjectsPointer.ItemButtonPointer[m_CurrentInfo.ItemCode].ItemCount.ToString();
            MainSystem.Instance.CAssetsPool.ObjectsPointer.ImageColorChange(m_CurrentInfo.ItemCode, MainSystem.Instance.CAssetsPool.ObjectsPointer.GamePlayItemImage);
            foreach (var l in m_MainSystem.CAssetsPool.Characters)
            {
                if (l.tag == "Player") l.SendMessage("SettingCurrentItem", m_CurrentInfo);
            }
        }

    }

    public void AddList(ItemInfo info, Sprite same)
    {
        if (ItemList.Count > 0)
        {
            foreach (var o in ItemList)
            {
                if (o.ItemIcon == same)
                {
                    ItemList.Remove(o);
                    break;
                }
            }
        }

        if (ItemList.Contains(info) == true)
            ItemList.Remove(info);

        if (ItemList.Count <= 0)
        {
            m_CurrentInfo = info;
            m_ObjectsPointer.GamePlayItemImage.sprite = m_CurrentInfo.ItemIcon;
            m_ObjectsPointer.GamePlayItemText.text = m_ObjectsPointer.ItemButtonPointer[m_CurrentInfo.ItemCode].ItemCount.ToString();
            foreach (var l in m_MainSystem.CAssetsPool.Characters)
            {
                if (l.tag == "Player")
                    l.SendMessage("SettingCurrentItem", m_CurrentInfo);
            }
        }

        ItemList.Add(info);
    }


}
