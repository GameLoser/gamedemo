﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class SetAreaName : MonoBehaviour
{
    private MainSystem m_MainSystem = null;

    [SerializeField]
    private string m_AreaName = "";
    [SerializeField]
    private bool IsBossArea = false;

    private void Start()
    {
        m_MainSystem = MainSystem.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if(IsBossArea == true)
            {
                m_MainSystem.CGameStateInput.IsStopPlayerInput = true;
                m_MainSystem.CAssetsPool.UISources[Window.GAMEPLAY_WINDOW].SetActive(false);
                m_MainSystem.CAssetsPool.UISources[Window.BLACK_FADE].SetActive(true);
                m_MainSystem.CAssetsPool.UISources[Window.BLACK_FADE].GetComponent<uTools.uTweenAlpha>().AddOnMiddle(()=>BossArea());
                m_MainSystem.CAssetsPool.ObjectsPointer.GamePlayMissionText.text = m_MainSystem.CRecordData.CurrentMission[3];
                this.GetComponent<Collider>().isTrigger = false;
            }
            else
            {
                m_MainSystem.CAssetsPool.ObjectsPointer.AreaName.text = m_AreaName;
                m_MainSystem.CAssetsPool.UISources[Window.AREA_WINDOW].SetActive(true);
                switch(m_AreaName)
                {
                    case "玄夜城前鎮":
                        m_MainSystem.CAssetsPool.ObjectsPointer.GamePlayMissionText.text = m_MainSystem.CRecordData.CurrentMission[2];
                        break;
                }
            }
        }
    }

    void BossArea()
    {
        m_MainSystem.CAssetsPool.TimeLineBoss.SendMessage("TargetMovieClip", MovieClip.BossEnter);
        m_MainSystem.CAssetsPool.MainCamera.gameObject.SetActive(false);
    }

}
